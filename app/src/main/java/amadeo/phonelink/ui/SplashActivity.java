package amadeo.phonelink.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.SecurityUtils;
import amadeo.phonelink.service.ForegroundService;
import amadeo.phonelink.ui.MainActivity;

/**
 * Created by Amadeo on 2018-01-26.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SecurityUtils.init(getApplicationContext());

        if (PhoneLinkUtils.firstRun(this)) {
            startActivity(new Intent(this, PermissionsActivity.class));
        } else
            startActivity(new Intent(getApplicationContext(), MainActivity.class));

        finish();
    }
}

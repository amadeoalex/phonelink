package amadeo.phonelink.ui;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import amadeo.phonelink.PermissionsUtils;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.R;
import amadeo.phonelink.service.ForegroundService;

public class PermissionsActivity extends AppCompatActivity {

    private Button continueButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);

        continueButton = findViewById(R.id.permissionsContinueButton);
        continueButton.setOnClickListener(listener -> {
            if (continueButton.getText().equals(getString(R.string.finish))) {
                PhoneLinkUtils.firstRunFinished(this);
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    PermissionsUtils.checkAndRequestPermissions(this);
                else
                    checkAndRequestNotificationAccess();
            }
        });
    }

    private void checkAndRequestNotificationAccess() {
        continueButton.setText(R.string.finish);
        PermissionsUtils.checkAndRequestNotificationAccess(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PermissionsUtils.REQUEST_RETURN_CODE) {
            checkAndRequestNotificationAccess();
        }
    }
}

package amadeo.phonelink.ui;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.lang.ref.WeakReference;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.R;
import amadeo.phonelink.ui.drawer.debug.DebugFragment;
import amadeo.phonelink.ui.drawer.featuresettings.FeatureSettingsFragment;
import amadeo.phonelink.ui.drawer.main.MainFragment;
import amadeo.phonelink.ui.drawer.pairing.PairingFragment;
import amadeo.phonelink.ui.drawer.settings.SettingsFragment;

/**
 * Created by Amadeo on 2018-06-26.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class SmoothActionBarDrawerToggle extends ActionBarDrawerToggle implements NavigationView.OnNavigationItemSelectedListener {
    private final String TAG = "PhoneLink Drawer";

    private final DrawerLayout drawerLayout;
    private final WeakReference<MainActivity> weakActivity;
    private final NavigationView navigationView;

    private int selectedItemId;

    private Runnable runnable;

    public SmoothActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, NavigationView navigationView) {
        super(activity, drawerLayout, toolbar, R.string.navigation_drawer_close, R.string.navigation_drawer_close);

        this.drawerLayout = drawerLayout;
        this.weakActivity = new WeakReference<>((MainActivity) activity);
        this.navigationView = navigationView;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//        PhoneLinkUtils.logI(TAG, "A");

        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);

        this.runnable = () -> switchFragmentByItemID(item.getItemId(), null, false);

        return true;
    }

    @Override
    public void onDrawerStateChanged(int newState) {
        super.onDrawerStateChanged(newState);

        if (runnable != null && newState == DrawerLayout.STATE_IDLE) {
            runnable.run();
            runnable = null;
        }
    }

    private void switchFragmentByItemID(Integer itemID, Fragment fragment, boolean force) {
//        PhoneLinkUtils.logI(TAG, "C %b", drawerLayout.isDrawerOpen(GravityCompat.START));
        if (itemID == null && fragment == null) {
            PhoneLinkUtils.logI(TAG, "itemID and fragment are both null");
            return;
        }

        if (itemID == null)
            itemID = getFragmentItemID(fragment);

        if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
//            PhoneLinkUtils.logI(TAG, "D");
            MainActivity activity = weakActivity.get();

            if (fragment == null)
                fragment = getFragmentFromItemID(itemID);

            if (fragment != null) {
                navigationView.setCheckedItem(itemID);
                setActionBarTitleFromItemID(itemID);

                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                Fragment currentFragment = fragmentManager.findFragmentByTag(fragment.getClass().getName());

                if (currentFragment == null || force) {
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                    fragmentTransaction.replace(R.id.content_main, fragment, fragment.getClass().getName());
                    fragmentTransaction.commit();
                } else {
                    PhoneLinkUtils.logI(TAG, "fragment already visible");
                }
            }
        }
    }

    private int getFragmentItemID(Fragment fragment) {
        if (fragment instanceof MainFragment)
            return R.id.nav_main;
        else if (fragment instanceof PairingFragment)
            return R.id.nav_pairing;
        else if (fragment instanceof FeatureSettingsFragment)
            return R.id.nav_feature_settings;
        else if (fragment instanceof SettingsFragment)
            return R.id.nav_settings;
        else if (fragment instanceof DebugFragment)
            return R.id.nav_debug;
        else
            return -1;
    }

    private Fragment getFragmentFromItemID(int itemID) {
        Fragment fragment = null;

        switch (itemID) {
            case R.id.nav_main:
                fragment = new MainFragment();
                break;

            case R.id.nav_pairing:
                fragment = new PairingFragment();
                break;

            case R.id.nav_feature_settings:
                fragment = new FeatureSettingsFragment();
                break;

            case R.id.nav_settings:
                fragment = new SettingsFragment();
                break;

            case R.id.nav_debug:
                fragment = new DebugFragment();
                break;
        }

        return fragment;
    }

    private void setActionBarTitleFromItemID(int itemID) {
        MainActivity activity = weakActivity.get();

        switch (itemID) {
            case R.id.nav_main:
                activity.setActionBarTitle(R.string.main_menu);
                break;

            case R.id.nav_pairing:
                activity.setActionBarTitle(R.string.pairing);
                break;

            case R.id.nav_feature_settings:
                activity.setActionBarTitle(R.string.features_settings);
                break;

            case R.id.nav_settings:
                activity.setActionBarTitle(R.string.title_activity_settings);
                break;

            case R.id.nav_debug:
                activity.setActionBarTitle(R.string.debug);
                break;
        }
    }

    public void switchFragment(Integer itemID, Fragment fragment) {
        switchFragmentByItemID(itemID, fragment, false);
    }

    public void switchFragment(Integer itemID, Fragment fragment, boolean force) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            this.runnable = () -> switchFragmentByItemID(itemID, fragment, force);
        } else
            switchFragmentByItemID(itemID, fragment, force);
    }
}



























package amadeo.phonelink.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.animation.AnimationUtils;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.R;
import amadeo.phonelink.service.ForegroundService;
import amadeo.phonelink.ui.drawer.main.MainFragment;
import amadeo.phonelink.ui.drawer.pairing.PairingFragment;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "PhoneLink MA";

    private SmoothActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        NavigationView navigationView = findViewById(R.id.nav_view);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        toggle = new SmoothActionBarDrawerToggle(this, drawer, toolbar, navigationView);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(toggle);

        if (savedInstanceState != null) {
            restoreSavedInstance(savedInstanceState);
        } else {
            if (!handleIntent(getIntent()))
                toggle.switchFragment(R.id.nav_main, new MainFragment());
        }

        ContextCompat.startForegroundService(this, new Intent(this, ForegroundService.class));
    }

    private void restoreSavedInstance(Bundle savedInstanceState) {
        String fragmentName = savedInstanceState.getString("active_fragment");
        PhoneLinkUtils.logI(TAG, "retrieved fragment: %s", fragmentName);

        try {
            Class<?> fragmentClass = Class.forName(fragmentName);
            Fragment fragment = (Fragment) fragmentClass.newInstance();

            toggle.switchFragment(null, fragment);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        handleIntent(intent);
    }

    private boolean handleIntent(Intent intent) {
        String action = intent.getAction();
        if (action != null) {
            PhoneLinkUtils.logI(TAG, "received new intent: %s", intent.getAction());

            switch (action) {
                case "amadeo.intent.action.PAIR_NOTIFICATION":
                    NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
                    notificationManagerCompat.cancel(ForegroundService.PAIR_NOTIFICATION_ID);

                    Fragment pairFragment = new PairingFragment();
                    pairFragment.setArguments(intent.getExtras());

                    toggle.switchFragment(R.id.nav_pairing, pairFragment, true);
                    return true;
                case "amadeo.intent.CONTENT":
                    Fragment mainFragment = new MainFragment();

                    toggle.switchFragment(R.id.nav_main, mainFragment);
                    return true;
                case "amadeo.intent.STOP":
                    ForegroundService.CreateWork(getApplicationContext(), service -> {
                        service.stopForeground(true);
                        service.stopSelf();
                    });

                    finish();
                    return true;
                default:
                    return false;
            }
        } else
            return false;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void setActionBarTitle(int resID) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle(resID);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.content_main);
        String fragmentName = currentFragment.getClass().getName();
        PhoneLinkUtils.logI(TAG, "stored fragment: %s", fragmentName);

        outState.putString("active_fragment", fragmentName);
    }
}

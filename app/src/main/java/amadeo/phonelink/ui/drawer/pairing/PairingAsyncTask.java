package amadeo.phonelink.ui.drawer.pairing;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.cert.CertificateEncodingException;
import java.util.concurrent.ThreadLocalRandom;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

import amadeo.phonelink.NetworkUtils;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.R;
import amadeo.phonelink.SecurityUtils;
import amadeo.phonelink.device.Computer;
import amadeo.phonelink.device.DeviceBase;
import amadeo.phonelink.device.DeviceManager;
import amadeo.phonelink.service.ForegroundService;
import amadeo.phonelink.service.network.NetPackage;

/**
 * Created by Amadeo on 2017-12-28.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class PairingAsyncTask extends AsyncTask<Void, Object, Void> {
    private final String TAG = PairingFragment.TAG;

    private final String PORT_CHECK_OK = "portCheckOK";

    private final WeakReference<Context> contextWeakReference;
    private final InetAddress inetAddress;
    private final WeakReference<TextView> instruction;
    private final WeakReference<ProgressBar> progressBar;

    private final boolean isClient;
    private final NetPackage netPackage;


    private SSLContext sslContext = null;
    private SSLServerSocket sslServerSocket = null;
    private SSLSocket sslSocket = null;

    private PrintWriter printWriter;
    private BufferedReader bufferedReader;

    public PairingAsyncTask(@NonNull Context context, @NonNull InetAddress inetAddress, boolean isClient, NetPackage netPackage, TextView instruction, ProgressBar progressBar) {
        this.contextWeakReference = new WeakReference<>(context);
        this.inetAddress = inetAddress;
        this.instruction = (instruction == null) ? null : new WeakReference<>(instruction);
        this.progressBar = (progressBar == null) ? null : new WeakReference<>(progressBar);

        this.isClient = isClient;
        this.netPackage = netPackage;
    }

    @Override
    protected void onPreExecute() {
        updateProgressBar(true);
        publishProgress(R.string.instruction_initializing);

        sslContext = SecurityUtils.getSSLContext(contextWeakReference.get(), null);
        if (sslContext == null)
            throw new NullPointerException("sslContext is null!");
    }

    private boolean setPairedDevice(NetPackage remoteDeviceIdPackage, NetPackage myIdPackage) throws UnknownHostException {
        if (remoteDeviceIdPackage != null && myIdPackage != null) {

            //add my port information to remote device id package
            remoteDeviceIdPackage.setInt(DeviceBase.KEY_LISTENING_TCP_PORT, myIdPackage.getInt(DeviceBase.KEY_TCP_PORT));

            String remoteDeviceId = remoteDeviceIdPackage.getString(DeviceBase.KEY_DEVICE_ID);
            byte[] remoteDeviceEncodedCertificate = android.util.Base64.decode(remoteDeviceIdPackage.getString(DeviceBase.KEY_CERTIFICATE), Base64.NO_WRAP);
            InetAddress remoteDeviceInetAddress = sslSocket.getInetAddress();
            int remoteDeviceTCPPort = remoteDeviceIdPackage.getInt(DeviceBase.KEY_TCP_PORT);
            int remoteDeviceLTCPPort = remoteDeviceIdPackage.getInt(DeviceBase.KEY_LISTENING_TCP_PORT);

            if (PhoneLinkUtils.inEmulator() && remoteDeviceInetAddress.getHostAddress().equals("10.0.2.2"))
                remoteDeviceInetAddress = InetAddress.getByName("192.168.1.100");

            WifiInfo wifiInfo = NetworkUtils.getWiFiInfo(contextWeakReference.get());
            if (wifiInfo != null) {
                DeviceBase computer = new Computer(remoteDeviceId, remoteDeviceEncodedCertificate, remoteDeviceInetAddress.getHostAddress(), remoteDeviceTCPPort, remoteDeviceLTCPPort, wifiInfo.getSSID(), wifiInfo.getBSSID());

                boolean added = DeviceManager.getInstance(contextWeakReference.get()).addDevice(computer);
                if (added) {
                    DeviceManager.getInstance(contextWeakReference.get()).initDevice(computer);
                }

                return true;
            } else {
                PhoneLinkUtils.logI(TAG, "wifiFifo is null!");
                return false;
            }
        } else {
            PhoneLinkUtils.logI(TAG, "deviceIdNetPackage is null!");
            return false;
        }
    }


    private void setupNetworkConnection() throws IOException {
        printWriter = new PrintWriter(sslSocket.getOutputStream(), true);
        bufferedReader = new BufferedReader(new InputStreamReader(sslSocket.getInputStream()));
    }

    @Override
    protected Void doInBackground(Void... voids) {
        publishProgress(R.string.instruction_waiting);

        try {
            if (isClient) {
                PhoneLinkUtils.logI(TAG, "trying to connect to %s at port %d", inetAddress.getHostAddress(), netPackage.getInt(DeviceBase.KEY_TCP_PORT));
                sslSocket = (SSLSocket) sslContext.getSocketFactory().createSocket(inetAddress, netPackage.getInt(DeviceBase.KEY_TCP_PORT));
                sslSocket.setSoTimeout(8000);
                sslSocket.startHandshake();

                setupNetworkConnection();
                publishProgress(R.string.instruction_pairing);

                //send identity package
                NetPackage myIdPackage = sendMyIdentityPackage();

                //get remote device identity package
                NetPackage remoteDeviceIdPackage = getRemoteDeviceIdentityPackage();

                if (setPairedDevice(remoteDeviceIdPackage, myIdPackage))
                    publishProgress(R.string.instruction_paired);
                else
                    publishProgress(R.string.instruction_error);
            } else {
                int tcpPort = PhoneLinkUtils.inEmulator() ? netPackage.getInt(DeviceBase.KEY_TCP_PORT) + 1 : netPackage.getInt(DeviceBase.KEY_TCP_PORT);

                PhoneLinkUtils.logI(TAG, "creating server at port %d", tcpPort);
                SSLServerSocketFactory sslServerSocketFactory = sslContext.getServerSocketFactory();

                sslServerSocket = (SSLServerSocket) sslServerSocketFactory.createServerSocket(tcpPort);
                sslServerSocket.setSoTimeout(8000);

                publishProgress(R.string.instruction_waiting);
                sslSocket = (SSLSocket) sslServerSocket.accept();
                sslSocket.startHandshake();

                setupNetworkConnection();
                publishProgress(R.string.instruction_pairing);

                //get remote device identity package
                NetPackage remoteDeviceIdPackage = getRemoteDeviceIdentityPackage();

                //send identity package
                NetPackage myIdPackage = sendMyIdentityPackage();

                if (setPairedDevice(remoteDeviceIdPackage, myIdPackage))
                    publishProgress(R.string.instruction_paired);
                else
                    publishProgress(R.string.instruction_error);
            }
        } catch (SocketTimeoutException e) {
            PhoneLinkUtils.logI(TAG, "SSLServerSocket timeout exception");
            publishProgress(R.string.instruction_socket_exception);
        } catch (SSLHandshakeException e) {
            PhoneLinkUtils.logI(TAG, "SSL exception!");
            publishProgress(R.string.instruction_error);
        } catch (IOException | CertificateEncodingException e) {
            e.printStackTrace();
        } finally {
            try {
                if (sslServerSocket != null && !sslServerSocket.isClosed())
                    sslServerSocket.close();

                if (sslSocket != null && sslSocket.isClosed())
                    sslSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Object... objects) {
        if (instruction != null) {
            Object object = objects[0];
            if (object instanceof Integer) {
                int resourceId = (Integer) objects[0];
                instruction.get().setText(contextWeakReference.get().getResources().getString(resourceId));
            } else if (object instanceof String) {
                String text = (String) objects[0];
                instruction.get().setText(text);
            }
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        updateProgressBar(false);
    }

    @Override
    protected void onCancelled() {
        try {
            if (sslServerSocket != null && !sslServerSocket.isClosed())
                sslServerSocket.close();

            if (sslSocket != null && sslSocket.isClosed())
                sslSocket.close();

            updateProgressBar(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateProgressBar(boolean show) {
        if (progressBar != null)
            progressBar.get().setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private NetPackage getMyIdNetPackage() throws CertificateEncodingException {

        int port;
        do {
            port = PhoneLinkUtils.getRandomInt(5555, 9999);
        }
        while (!DeviceManager.getInstance(contextWeakReference.get()).checkPortAvailability(port));

        NetPackage deviceIdPackage = new NetPackage("");
        deviceIdPackage.setString(DeviceBase.KEY_DEVICE_ID, Build.MODEL);
        deviceIdPackage.setInt(DeviceBase.KEY_TCP_PORT, port);
        deviceIdPackage.setString(DeviceBase.KEY_TYPE, PhoneLinkUtils.getDeviceType(contextWeakReference.get()));
        deviceIdPackage.setString(DeviceBase.KEY_CERTIFICATE, android.util.Base64.encodeToString(SecurityUtils.getDeviceCertificate().getEncoded(), Base64.NO_WRAP));

        return deviceIdPackage;
    }

    private NetPackage sendMyIdentityPackage() throws CertificateEncodingException, IOException {
        NetPackage myIdPackage;
        do {
            //send my identity package
            myIdPackage = getMyIdNetPackage();
            printWriter.println(myIdPackage.toString());
        } while (!bufferedReader.readLine().equals(PORT_CHECK_OK));

        return myIdPackage;
    }

    private NetPackage getRemoteDeviceIdentityPackage() throws IOException {
        NetPackage remoteDeviceIdPackage;
        while (true) {
            remoteDeviceIdPackage = NetPackage.deserialize(bufferedReader.readLine());
            //System.out.println(remoteDeviceIdPackage);

            //verify port availability
            if (DeviceManager.getInstance(contextWeakReference.get()).checkPortAvailability(remoteDeviceIdPackage.getInt(DeviceBase.KEY_TCP_PORT))) {
                printWriter.println(PORT_CHECK_OK);
                break;
            } else {
                printWriter.println(" ");
            }
        }

        return remoteDeviceIdPackage;
    }
}

package amadeo.phonelink.ui.drawer.main;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.MenuInflater;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;

import amadeo.phonelink.R;
import amadeo.phonelink.device.DeviceBase;
import amadeo.phonelink.device.DeviceManager;
import amadeo.phonelink.service.ForegroundService;

/**
 * Created by Amadeo on 2018-07-04.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class DeviceHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
    private final String TAG = MainFragment.TAG;

    private final Context context;

    private final View itemView;
    private final TextView deviceIp;
    private final TextView deviceId;
    private final TextView connectedStatus;
    private final DeviceChangeCallback callback;

    public DeviceHolder(Context context, View itemView, DeviceChangeCallback callback) {
        super(itemView);

        this.context = context;
        this.itemView = itemView;

        this.deviceId = itemView.findViewById(R.id.mainDeviceId);
        this.deviceIp = itemView.findViewById(R.id.mainDeviceIp);
        this.connectedStatus = itemView.findViewById(R.id.mainConnected);
        this.callback = callback;

        this.itemView.setOnLongClickListener(this);
    }

    public void bindDevice(DeviceBase device) {
        this.deviceId.setText(device.getDeviceId());
        String ipString = String.format("(%s)", device.getAddress());
        this.deviceIp.setText(ipString);

        changeConnectedStatus(device.getConnectionServer().isConnected());
    }


    @Override
    public boolean onLongClick(View view) {
        PopupMenu popupMenu = new PopupMenu(context, view);
        MenuInflater menuInflater = popupMenu.getMenuInflater();
        popupMenu.setOnMenuItemClickListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.menu_main_computer_disconnect: {
                    new Thread(() -> {
                        DeviceBase device = DeviceManager.getInstance(context).getPairedDevice((String) deviceId.getText());
                        int messageID = device.getConnectionServer().disconnect() ? R.string.disconnected : R.string.not_connected;
                        Snackbar.make(view, messageID, Snackbar.LENGTH_SHORT).show();
                    }).start();
                    return true;
                }
                case R.id.menu_main_computer_unpair: {
                    DeviceBase computer = DeviceManager.getInstance(context).getPairedDevice((String) deviceId.getText());
                    new Thread(() -> {
                        if (DeviceManager.getInstance(context).removePairedDevice(computer)) {
                            Snackbar snackbar = Snackbar.make(view, R.string.device_unpaired, Snackbar.LENGTH_LONG)
                                    .setAction(R.string.undo, view1 -> {
                                        DeviceManager.getInstance(context).addDevice(computer);
                                        DeviceManager.getInstance(context).initDevice(computer);
                                        callback.add(computer);
                                    })
                                    .setActionTextColor(context.getResources().getColor(R.color.colorAccent));
                            snackbar.show();
                        }
                    }
                    ).start();

                    callback.remove(getAdapterPosition());

                    return true;
                }
                case R.id.menu_main_computer_details: {
                        DeviceBase computer = DeviceManager.getInstance(context).getPairedDevice((String) deviceId.getText());
                        String message = context.getResources().getString(R.string.details_message, computer.getDeviceId(), computer.getAddress(), computer.getTcpPort(), computer.getListeningTcpPort(), computer.getbSSID(), computer.getSSID());

                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle(R.string.details)
                                .setMessage(message)
                                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                                });
                        builder.create().show();
                }

                default:
                    return false;
            }
        });

        menuInflater.inflate(R.menu.menu_fragment_main_card, popupMenu.getMenu());
        popupMenu.show();

        return true;
    }

    public void changeConnectedStatus(boolean connected) {
        if (connected) {
            connectedStatus.setText(R.string.yes);
            connectedStatus.setTextColor(Color.argb(255, 100, 255, 0));
        } else {
            connectedStatus.setText(R.string.no);
            connectedStatus.setTextColor(Color.argb(255, 255, 100, 0));
        }
    }
}

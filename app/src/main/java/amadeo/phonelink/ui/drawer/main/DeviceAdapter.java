package amadeo.phonelink.ui.drawer.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.device.DeviceBase;

/**
 * Created by Amadeo on 2018-07-04.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class DeviceAdapter extends RecyclerView.Adapter<DeviceHolder> implements DeviceChangeCallback {
    private final String TAG = MainFragment.TAG;

    private final Context context;
    private final ArrayList<DeviceBase> devices;
    private final int itemResource;

    public DeviceAdapter(Context context, ArrayList<DeviceBase> devices, int itemResource) {
        this.context = context;
        this.devices = devices;
        this.itemResource = itemResource;
    }

    @NonNull
    @Override
    public DeviceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(itemResource, parent, false);

        return new DeviceHolder(context, view, this);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceHolder holder, int position) {
        DeviceBase device = devices.get(position);
        holder.bindDevice(device);
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    @Override
    public void add(DeviceBase device) {
        devices.add(device);
        notifyItemInserted(devices.size() - 1);
    }

    @Override
    public void remove(int position) {
        devices.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, devices.size());
    }

    public int getIndex(DeviceBase device) {
        if (devices.contains(device)) {
            PhoneLinkUtils.logI(TAG, "returning index %d", devices.indexOf(device));
            return devices.indexOf(device);
        }

        return -1;
    }
}

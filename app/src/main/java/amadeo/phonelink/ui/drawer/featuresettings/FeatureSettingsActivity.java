package amadeo.phonelink.ui.drawer.featuresettings;

import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

import amadeo.phonelink.features.Feature;

public class FeatureSettingsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String featureName = bundle.getString(Feature.ARG_NAME, "<unknown>");
            String featureDisplayName = bundle.getString(Feature.ARG_DISPLAY_NAME, "<unknown>");

            int resourceId = getResources().getIdentifier(featureName.toLowerCase() + "_preferences", "xml", getPackageName());
            PreferenceFragment preferenceFragment = new PreferenceFragment();
            Bundle fragmentBundle = new Bundle();
            fragmentBundle.putInt("resourceId", resourceId);
            preferenceFragment.setArguments(fragmentBundle);


            getSupportFragmentManager().beginTransaction().replace(android.R.id.content, preferenceFragment).commit();
            ActionBar supportActionBar = getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.setTitle(featureDisplayName);
                supportActionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();

        return true;
    }

    public static class PreferenceFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            Bundle bundle = getArguments();

            if (bundle != null)
                addPreferencesFromResource(getArguments().getInt("resourceId"));
        }
    }
}

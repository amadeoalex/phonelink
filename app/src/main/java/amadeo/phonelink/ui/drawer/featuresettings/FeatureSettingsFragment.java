package amadeo.phonelink.ui.drawer.featuresettings;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import amadeo.phonelink.R;

/**
 * Created by Amadeo on 2018-01-19.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class FeatureSettingsFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feature_settings, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.featureSettingsRecyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        FeatureAdapter adapter = new FeatureAdapter(getActivity(), R.layout.fragment_feature_settings_row);
        recyclerView.setAdapter(adapter);

        return view;
    }

}

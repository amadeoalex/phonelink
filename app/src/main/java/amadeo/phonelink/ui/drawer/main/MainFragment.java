package amadeo.phonelink.ui.drawer.main;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.R;
import amadeo.phonelink.device.DeviceBase;
import amadeo.phonelink.device.DeviceManager;
import amadeo.phonelink.service.ForegroundService;

/**
 * Created by Amadeo on idk.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class MainFragment extends Fragment implements RecyclerView.OnChildAttachStateChangeListener {
    public static final String TAG = "PhoneLink MainFragment";

    public static final String INTENT_CONNECTION_CHANGE = "connectionChange";

    private BroadcastReceiver broadcastReceiver;

    private TextView notConnectedLabel;

    private RecyclerView recyclerView;
    private DeviceAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateUI(intent);
            }
        };

        FragmentActivity activity = getActivity();
        if (activity != null)
            LocalBroadcastManager.getInstance(activity.getApplicationContext()).registerReceiver(broadcastReceiver, new IntentFilter(INTENT_CONNECTION_CHANGE));
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        notConnectedLabel = view.findViewById(R.id.mainNotConnectedLabel);

        recyclerView = view.findViewById(R.id.mainRecyclerView);
        adapter = new DeviceAdapter(getActivity(), new ArrayList<>(), R.layout.fragment_main_row_card);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.addOnChildAttachStateChangeListener(this);

        ArrayList<DeviceBase> devices = DeviceManager.getInstance(getActivity().getApplicationContext()).getPairedDevices();
        if (!devices.isEmpty()) {
            notConnectedLabel.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            for (DeviceBase computer : devices)
                adapter.add(computer);
        }

        return view;
    }

    private void updateUI(final Intent intent) {
        String deviceID = intent.getStringExtra(DeviceBase.KEY_DEVICE_ID);
        PhoneLinkUtils.logI(TAG, "intent: %s, deviceID: %s", intent, deviceID);

        DeviceBase device = DeviceManager.getInstance(getActivity().getApplicationContext()).getPairedDevice(deviceID);
        if (device != null)
            adapter.notifyItemChanged(adapter.getIndex(device));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        FragmentActivity activity = getActivity();
        if (activity != null)
            LocalBroadcastManager.getInstance(activity.getApplicationContext()).unregisterReceiver(broadcastReceiver);
        else
            PhoneLinkUtils.logW(TAG, "getActivity returned null in onDestroy");
    }

    @Override
    public void onChildViewAttachedToWindow(View view) {
        if (adapter.getItemCount() > 0)
            notConnectedLabel.setVisibility(View.GONE);

    }

    @Override
    public void onChildViewDetachedFromWindow(View view) {
        if (adapter.getItemCount() == 0)
            notConnectedLabel.setVisibility(View.VISIBLE);
    }
}

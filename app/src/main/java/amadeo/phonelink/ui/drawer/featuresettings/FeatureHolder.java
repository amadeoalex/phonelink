package amadeo.phonelink.ui.drawer.featuresettings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.TextView;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.R;
import amadeo.phonelink.features.Feature;
import amadeo.phonelink.features.FeatureManager;

/**
 * Created by Amadeo on 2018-01-21.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

class FeatureHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final String TAG = "PhoneLink FeatureHolder";

    private final Context context;
    private final TextView featureNameTextView;
    private final SwitchCompat enabledSwitch;
    private String featureName;

    public FeatureHolder(Context context, View itemView) {
        super(itemView);

        this.context = context;
        this.featureNameTextView = itemView.findViewById(R.id.settingsFeatureName);
        this.enabledSwitch = itemView.findViewById(R.id.settingsFeatureEnabledSwitch);

        itemView.setOnClickListener(this);
    }

    public void bindFeature(String featureName) {
        this.featureName = featureName;

        featureNameTextView.setText(FeatureManager.getFeature(featureName).getDisplayName());
        if (FeatureManager.isEnabled(featureName))
            enabledSwitch.setChecked(true);
        else
            enabledSwitch.setChecked(false);

        enabledSwitch.setOnClickListener(view -> {
            if (enabledSwitch.isChecked()) {
                FeatureManager.enableFeature(featureName);
            } else {
                FeatureManager.disableFeature(featureName);
            }
        });
    }

    @Override
    public void onClick(View view) {
        PhoneLinkUtils.logI(TAG, "Clicked on %s", featureName);

//        AppCompatActivity activity = (AppCompatActivity) view.getContext();
//        Fragment myFragment = new FeatureSettingsFragment.FeatureSettingsFragment();
//        activity.getSupportFragmentManager().beginTransaction().replace(R.id.featureSettingsContainer, myFragment).commit();

        Feature feature = FeatureManager.getFeature(featureName);
        if (feature.hasSettings()) {
            Intent intent = new Intent(context, FeatureSettingsActivity.class);
            Bundle bundle = new Bundle();
            String displayName = feature.getDisplayName();
            bundle.putString(Feature.ARG_NAME, featureName);
            bundle.putString(Feature.ARG_DISPLAY_NAME, displayName);
            intent.putExtras(bundle);

            context.startActivity(intent);
        } else {
            Snackbar.make(view, context.getResources().getString(R.string.feature_has_no_settings, feature.getDisplayName()), Snackbar.LENGTH_SHORT).show();
        }
    }
}

package amadeo.phonelink.ui.drawer.pairing;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import amadeo.phonelink.service.network.NetPackage;

/**
 * Created by Amadeo on 2017-12-17.
 */

public class DeviceAdapter extends RecyclerView.Adapter<DeviceHolder> {
    private final String TAG = PairingFragment.TAG;

    private final Context context;
    private final ArrayList<NetPackage> devices;
    private final int itemResource;

    public DeviceAdapter(Context context, ArrayList<NetPackage> devices, int itemResource) {
        this.context = context;
        this.devices = devices;
        this.itemResource = itemResource;
    }

    @NonNull
    @Override
    public DeviceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(itemResource, parent, false);

        return new DeviceHolder(context, view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceHolder holder, int position) {
        NetPackage netPackage = devices.get(position);
        holder.bindDevice(netPackage);
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    public void add(NetPackage netPackage) {
        devices.add(netPackage);
        notifyItemInserted(devices.size() - 1);
    }

    public void clear() {
        devices.clear();
        notifyDataSetChanged();
    }
}

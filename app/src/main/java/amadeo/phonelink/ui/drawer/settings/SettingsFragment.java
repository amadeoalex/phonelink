package amadeo.phonelink.ui.drawer.settings;

import android.app.Notification;
import android.app.NotificationManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;

import amadeo.phonelink.R;
import amadeo.phonelink.service.ForegroundService;

/**
 * Created by Amadeo on 2018-01-26.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class SettingsFragment extends PreferenceFragmentCompat {
    public SettingsFragment() {
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.settings_preferences);

        Preference showNotification = findPreference("pref_key_show_notification_icon");
        showNotification.setOnPreferenceChangeListener((preference, newValue) -> {
            ForegroundService.CreateWork(getActivity(), service -> {
                int priority = (boolean) newValue ? NotificationCompat.PRIORITY_LOW : NotificationCompat.PRIORITY_MIN;
                service.notification = service.notificationCompatBuilder.setPriority(priority).build();
                service.notificationManager.notify(ForegroundService.MAIN_NOTIFICATION_ID, service.notification);
            });

            return true;
        });
    }
}

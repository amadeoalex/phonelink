package amadeo.phonelink.ui.drawer.featuresettings;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import amadeo.phonelink.features.FeatureManager;

/**
 * Created by Amadeo on 2018-01-21.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class FeatureAdapter extends RecyclerView.Adapter<FeatureHolder> {
    private final Context context;
    private final int itemResource;
    private final ArrayList<String> featureNames = new ArrayList<>(FeatureManager.getFeatures());

    public FeatureAdapter(Context context, int itemResource) {
        this.context = context;
        this.itemResource = itemResource;
    }

    @NonNull
    @Override
    public FeatureHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(itemResource, parent, false);

        return new FeatureHolder(context, view);
    }

    @Override
    public void onBindViewHolder(@NonNull FeatureHolder holder, int position) {
        String featureName = featureNames.get(position);
        holder.bindFeature(featureName);
    }

    @Override
    public int getItemCount() {
        return featureNames.size();
    }

}

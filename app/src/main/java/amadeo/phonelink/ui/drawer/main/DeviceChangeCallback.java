package amadeo.phonelink.ui.drawer.main;


import amadeo.phonelink.device.DeviceBase;

/**
 * Created by Amadeo on 2018-07-06.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public interface DeviceChangeCallback {
    void add(DeviceBase computer);

    void remove(int position);
}

package amadeo.phonelink.ui.drawer.pairing;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.R;
import amadeo.phonelink.device.DeviceBase;
import amadeo.phonelink.device.DeviceManager;
import amadeo.phonelink.service.ForegroundService;
import amadeo.phonelink.service.network.NetPackage;
import amadeo.phonelink.service.network.UDPServer;

/**
 * Created by Amadeo on 2017-12-17.
 */

public class DeviceHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final String TAG = PairingFragment.TAG;

    private final TextView ip;
    private final TextView instruction;
    private final ImageView icon;
    private final ProgressBar progressBar;

    private InetAddress inetAddress;
    private NetPackage netPackage;
    private boolean isClient;

    private PairingAsyncTask asyncTask;

    private final Context context;

    public DeviceHolder(Context context, View itemView) {
        super(itemView);

        this.context = context;
        this.ip = itemView.findViewById(R.id.pairingIP);
        this.instruction = itemView.findViewById(R.id.pairingInstruction);
        this.icon = itemView.findViewById(R.id.pairingIcon);
        this.progressBar = itemView.findViewById(R.id.pairingProgressBar);

        itemView.setOnClickListener(this);


        PhoneLinkUtils.logI(TAG, "creating holder");
    }

    public void bindDevice(NetPackage netPackage) {
        this.ip.setText(netPackage.getString(DeviceBase.KEY_DEVICE_ID));
        this.instruction.setText("");
        this.instruction.setVisibility(View.GONE);
        this.icon.setImageResource(R.drawable.ic_computer);

        this.netPackage = netPackage;
        this.isClient = netPackage.getInt("isClient") == 1;

        try {
            this.inetAddress = InetAddress.getByName(netPackage.getString(DeviceBase.KEY_INET_ADDRESS));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        if (DeviceManager.getInstance(context).isPaired(inetAddress)) {
            PhoneLinkUtils.logI(TAG, "device already paired");
            instruction.setVisibility(View.VISIBLE);
            instruction.setText(R.string.instruction_already_paired);
        } else {
            PhoneLinkUtils.logI(TAG, "starting pairing process");
            startPairing();
        }
    }

    private void startPairing() {
        if (asyncTask != null && asyncTask.getStatus() != AsyncTask.Status.RUNNING) {
            PhoneLinkUtils.logI(TAG, "async task not running, nulling");
            asyncTask = null;
        }

        if (asyncTask == null) {
            instruction.setVisibility(View.VISIBLE);

            if (isClient) {
                asyncTask = new PairingAsyncTask(context, inetAddress, true, netPackage, instruction, progressBar);
                //asyncTask.execute();
                asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR); //network scan blocking "async task" thread causes troubles with ssl pairing
            } else {
                NetPackage pairRequestPackage = new NetPackage(UDPServer.TYPE_PAIR);
                pairRequestPackage.setString(DeviceBase.KEY_DEVICE_ID, Build.MODEL);
                pairRequestPackage.setInt(DeviceBase.KEY_TCP_PORT, 6162); //TODO: randomize
                pairRequestPackage.setString(DeviceBase.KEY_TYPE, DeviceBase.TYPE_PHONE);

                asyncTask = new PairingAsyncTask(context, inetAddress, false, pairRequestPackage, instruction, progressBar);
                //asyncTask.execute();
                asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR); //network scan blocking "async task" thread causes troubles with ssl pairing

                new Thread(() -> {
                    try {
                        byte[] buffer = pairRequestPackage.toString().getBytes();
                        DatagramPacket pairRequestPacket = new DatagramPacket(buffer, buffer.length, inetAddress, 4848);

                        DatagramSocket datagramSocket = new DatagramSocket();
                        PhoneLinkUtils.logI(TAG, "sending to %s", inetAddress.getHostAddress());
                        datagramSocket.send(pairRequestPacket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        } else {
            PhoneLinkUtils.logI(TAG, "async task already running!");
        }
    }
}
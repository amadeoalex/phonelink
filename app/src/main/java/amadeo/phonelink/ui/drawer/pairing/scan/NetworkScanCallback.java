package amadeo.phonelink.ui.drawer.pairing.scan;

/**
 * Created by Amadeo on 2018-06-29.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public interface NetworkScanCallback {
    void onScanFinished();
}
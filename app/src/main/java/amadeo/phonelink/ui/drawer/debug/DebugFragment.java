package amadeo.phonelink.ui.drawer.debug;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import amadeo.phonelink.device.DeviceBase;
import amadeo.phonelink.device.DeviceManager;
import amadeo.phonelink.device.network.DeviceManagerHelper;
import amadeo.phonelink.service.ForegroundService;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.R;
import amadeo.phonelink.features.smsnotifications.ContactsManager;
import amadeo.phonelink.service.NotificationListener;
import amadeo.phonelink.service.network.NetPackage;

/**
 * A simple {@link Fragment} subclass.
 */
public class DebugFragment extends Fragment {
    private static final String TAG = "PhoneLink DF";

    private EditText netPackageNameEditText;
    private Spinner spinner;
    private ArrayAdapter<String> spinnerAdapter;

    public DebugFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug, container, false);

        netPackageNameEditText = view.findViewById(R.id.debugPacketNameTextEdit);
        spinner = view.findViewById(R.id.debugSpinner);
        FragmentActivity fragmentActivity = getActivity();
        if (fragmentActivity != null)
            spinnerAdapter = new ArrayAdapter<>(fragmentActivity, android.R.layout.simple_spinner_dropdown_item, new ArrayList<>());
        else
            throw new NullPointerException("getActivity() returned null!");

        spinnerAdapter.add("All");
        for (DeviceBase device : DeviceManager.getInstance(getActivity()).getPairedDevices()) {
            spinnerAdapter.add(device.getDeviceId());
        }

        spinner.setAdapter(spinnerAdapter);

        prepareButtons(view);
        return view;
    }


    private void prepareButtons(View view) {
        FragmentActivity fragmentActivity = getActivity();

        Button netPackageSendButton = view.findViewById(R.id.debugButtonSend);
        netPackageSendButton.setOnClickListener(view1 -> {
            String selection = ((TextView) spinner.getSelectedView()).getText().toString();
            NetPackage netPackage = new NetPackage(netPackageNameEditText.getText().toString().trim());
            if (selection.equals("All")) {
                DeviceManagerHelper.sendNetPackageToAll(getActivity(), netPackage);
            } else {
                DeviceBase device = DeviceManager.getInstance(getActivity()).getPairedDevice(selection);
                if (device != null)
                    device.getConnectionServer().sendNetPackage(netPackage);
            }
            if (fragmentActivity != null) {
                InputMethodManager imm = (InputMethodManager) fragmentActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });

        if (fragmentActivity != null) {
            view.findViewById(R.id.debugButtonConv).setOnClickListener(listener -> {
                HashMap map = ContactsManager.getAllConversations(fragmentActivity);
            });

            view.findViewById(R.id.debugButtonDisc).setOnClickListener(listener -> {
                new Thread(() -> DeviceManager.getInstance(getActivity()).getPairedDevice(0).getConnectionServer().disconnect()).start();
            });

            view.findViewById(R.id.debugButtonFS).setOnClickListener(listener -> {
                Intent intent = new Intent(fragmentActivity, ForegroundService.class);
                ContextCompat.startForegroundService(fragmentActivity, intent);
            });

            view.findViewById(R.id.debugButtonStopFS).setOnClickListener(listener -> {
                ForegroundService.CreateWork(fragmentActivity, service -> {
                    service.stopForeground(true);
                    service.stopSelf();
                });
            });

            view.findViewById(R.id.debugButtonTest).setOnClickListener(listener -> {
                //ContactsManager.getConversationFrom(fragmentActivity, "663618400", null);

                Intent intent = new Intent(NotificationListener.BROADCAST_ACTION);
                LocalBroadcastManager.getInstance(fragmentActivity).sendBroadcast(intent);
            });
        } else
            throw new NullPointerException("getActivity() returned null!");
    }

}

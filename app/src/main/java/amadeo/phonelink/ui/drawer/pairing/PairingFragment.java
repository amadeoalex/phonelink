package amadeo.phonelink.ui.drawer.pairing;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import amadeo.phonelink.NetworkUtils;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.R;
import amadeo.phonelink.service.network.NetPackage;
import amadeo.phonelink.ui.drawer.pairing.scan.NetworkScanAsyncTask;
import amadeo.phonelink.ui.drawer.pairing.scan.NetworkScanCallback;

public class PairingFragment extends Fragment implements NetworkScanCallback {
    public static final String TAG = "PhoneLink PF";

    private RecyclerView recyclerView;
    private DeviceAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    private NetworkScanAsyncTask networkScan;

    public PairingFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pairing, container, false);

        recyclerView = view.findViewById(R.id.pairingRecyclerView);
        adapter = new DeviceAdapter(getActivity(), new ArrayList<>(), R.layout.fragment_pairing_row_card);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        swipeRefreshLayout = view.findViewById(R.id.pairingSwipeLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(() -> performNetworkScan(getActivity(), adapter));

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_pairing, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_pair_refresh:
                swipeRefreshLayout.setRefreshing(true);
                performNetworkScan(getActivity(), adapter);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments != null) {
            if (arguments.containsKey("netPackage")) {
                PhoneLinkUtils.logI(TAG, "adding device");
                NetPackage netPackage = NetPackage.deserialize(arguments.getString("netPackage"));
                adapter.add(netPackage);

                PhoneLinkUtils.logI(TAG, "auto click");

                recyclerView.postDelayed(() -> {
                    DeviceHolder itemHolder = (DeviceHolder) recyclerView.findViewHolderForAdapterPosition(0);
                    if (itemHolder != null)
                        itemHolder.itemView.performClick();
                }, 50);
            }
        } else {
            performNetworkScan(getActivity(), adapter);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (networkScan != null)
            networkScan.cancel(true);
    }

    private void performNetworkScan(Context context, DeviceAdapter adapter) {
        if (NetworkUtils.connectedToWiFi(context)) {
            if (networkScan != null && networkScan.getStatus() != AsyncTask.Status.RUNNING) {
                PhoneLinkUtils.logI(TAG, "network scan is not running, nulling");
                networkScan = null;
            }

            if (networkScan == null) {
                networkScan = new NetworkScanAsyncTask(context, adapter, this);
                networkScan.execute();
            } else {
                PhoneLinkUtils.logI(TAG, "network scan is already running!");
            }
        } else {
            swipeRefreshLayout.setRefreshing(false);
            Activity activity = getActivity();
            if (activity != null) {
                Snackbar snackbar = Snackbar.make(activity.findViewById(R.id.rootCoordinatorLayout), R.string.wifi_unreachable, Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        }
    }

    @Override
    public void onScanFinished() {
        swipeRefreshLayout.setRefreshing(false);
    }
}

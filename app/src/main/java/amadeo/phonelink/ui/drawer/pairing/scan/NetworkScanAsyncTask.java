package amadeo.phonelink.ui.drawer.pairing.scan;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.Toast;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Enumeration;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.device.DeviceBase;
import amadeo.phonelink.service.network.NetPackage;
import amadeo.phonelink.ui.drawer.pairing.DeviceAdapter;
import amadeo.phonelink.ui.drawer.pairing.PairingFragment;

/**
 * Created by Amadeo on 2017-12-17.
 */

public class NetworkScanAsyncTask extends AsyncTask<Void, NetPackage, Void> {
    private final String TAG = PairingFragment.TAG;

    private final String TYPE_LOOKUP = "amadeo.phonelink.package.LOOKUP";
    private final String TYPE_LOOKUP_RESPONSE = "amadeo.phonelink.package.LOOKUP_RESPONSE";

    private final WeakReference<Context> contextWeakReference;
    private final DeviceAdapter adapter;
    private final NetworkScanCallback callback;

    public NetworkScanAsyncTask(Context context, DeviceAdapter adapter, NetworkScanCallback callback) {
        this.contextWeakReference = new WeakReference<>(context);
        this.adapter = adapter;
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        adapter.clear();
        adapter.notifyItemRangeChanged(0, adapter.getItemCount());
    }

    @Override
    protected Void doInBackground(Void... voids) {
        DatagramSocket socket = null;

        try {
            ArrayList<InetAddress> broadcastAddresses = new ArrayList<>();
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface networkInterface = interfaces.nextElement();
                if (networkInterface.isLoopback())
                    continue;

                for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {
                    InetAddress broadcastAddress = interfaceAddress.getBroadcast();
                    if (broadcastAddress != null) {
                        broadcastAddresses.add(broadcastAddress);
                    }
                }
            }

            if (broadcastAddresses.size() == 0) {
                PhoneLinkUtils.logW(TAG, "fallback to '192.168.1.255'");
                broadcastAddresses.add(InetAddress.getByName("192.168.1.255"));
            }

            socket = new DatagramSocket();
            socket.setSoTimeout(3000);
            socket.setBroadcast(true);

            NetPackage lookupPackage = new NetPackage(TYPE_LOOKUP);
            byte[] lookupBuffer = lookupPackage.toString().getBytes();

            for (InetAddress address : broadcastAddresses) {
                PhoneLinkUtils.logI(TAG, "sending to %s", address.getHostAddress());
                DatagramPacket lookupPacket = new DatagramPacket(lookupBuffer, lookupBuffer.length, address, 4848);
                socket.send(lookupPacket);
            }

            byte[] responseBuffer = new byte[1024 * 512];
            DatagramPacket responsePacket = new DatagramPacket(responseBuffer, responseBuffer.length);

            boolean running = true;
            while (running) {
                try {
                    PhoneLinkUtils.logI(TAG, "waiting for response");
                    socket.receive(responsePacket);

                    PhoneLinkUtils.logI(TAG, "received response");
                    NetPackage responseNetPackage = NetPackage.deserialize(new String(responsePacket.getData()).trim());
                    if (responseNetPackage != null && responseNetPackage.type().equals(TYPE_LOOKUP_RESPONSE) && !responseNetPackage.getString(DeviceBase.KEY_DEVICE_ID).equals(Build.MODEL)) {
                        responseNetPackage.setString(DeviceBase.KEY_INET_ADDRESS, responsePacket.getAddress().getHostAddress());

                        publishProgress(responseNetPackage);
                    }
                } catch (SocketTimeoutException e) {
                    running = false;
                    PhoneLinkUtils.logI(TAG, "socket timeout exception");
                }
            }
        } catch (ConnectException e){
            PhoneLinkUtils.logI(TAG, "connect exceptions");
        } catch (SocketException e) {
            PhoneLinkUtils.logI(TAG, "socket exception");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (socket != null && !socket.isClosed()) {
                socket.close();
            }
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(NetPackage... netPackage) {
        adapter.add(netPackage[0]);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        Toast.makeText(contextWeakReference.get(), "done?", Toast.LENGTH_SHORT).show();
        callback.onScanFinished();
    }
}
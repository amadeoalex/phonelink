package amadeo.phonelink;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Base64;

import org.spongycastle.asn1.x500.X500NameBuilder;
import org.spongycastle.asn1.x500.style.BCStyle;
import org.spongycastle.cert.X509CertificateHolder;
import org.spongycastle.cert.X509v3CertificateBuilder;
import org.spongycastle.cert.jcajce.JcaX509CertificateConverter;
import org.spongycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.spongycastle.jce.provider.BouncyCastleProvider;
import org.spongycastle.operator.ContentSigner;
import org.spongycastle.operator.OperatorCreationException;
import org.spongycastle.operator.jcajce.JcaContentSignerBuilder;

import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyManagementException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import amadeo.phonelink.device.DeviceBase;

/**
 * Created by Amadeo on 2018-06-23.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class SecurityUtils {
    private static final String TAG = "PhoneLink SecUtils";

    private static final BouncyCastleProvider bouncyCastleProvider = new BouncyCastleProvider();

    private static final Random random = new Random(System.currentTimeMillis());
    private static final String GEN_CHARACTERS = "a1bZc2dYe3fXg4hWi5jVk6lUm7nTo8pSr9sRt9uQw8yPz7qO6N5M4L3K2J1I0H1G0F2E0D3C0B4A";

    private static X509Certificate deviceCertificate = null;

    public static synchronized void init(Context context) {
        if (deviceCertificate != null)
            return;

        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            //Check device RSA key pair
            if (!sharedPreferences.contains("publicKey") || !sharedPreferences.contains("privateKey")) {
                PhoneLinkUtils.logI(TAG, "generating device key pair");

                KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
                keyPairGenerator.initialize(2048);
                KeyPair keyPair = keyPairGenerator.generateKeyPair();

                String publicKey = Base64.encodeToString(keyPair.getPublic().getEncoded(), 0);
                String privateKey = Base64.encodeToString(keyPair.getPrivate().getEncoded(), 0);

                editor.putString("publicKey", publicKey);
                editor.putString("privateKey", privateKey);
                editor.apply();
            }

            PublicKey publicKey = getPublicKey(context);
            PrivateKey privateKey = getPrivateKey(context);

            //TODO: save the certificate?
            if (!sharedPreferences.contains(DeviceBase.KEY_CERTIFICATE)) {
                PhoneLinkUtils.logI(TAG, "generating new certificate");
                deviceCertificate = generateCertificate(Build.MODEL, publicKey, privateKey, "SHA256withRSA");

                sharedPreferences.edit().putString(DeviceBase.KEY_CERTIFICATE, Base64.encodeToString(deviceCertificate.getEncoded(),0)).apply();
            } else {
                byte[] deviceEncodedCertificate = Base64.decode(sharedPreferences.getString(DeviceBase.KEY_CERTIFICATE, ""), 0);
                X509CertificateHolder certificateHolder = new X509CertificateHolder(deviceEncodedCertificate);

                deviceCertificate = new JcaX509CertificateConverter().setProvider(bouncyCastleProvider).getCertificate(certificateHolder);
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | CertificateException | IOException e) {
            e.printStackTrace();
        }
    }

    public static PublicKey getPublicKey(Context context) throws
            InvalidKeySpecException, NoSuchAlgorithmException {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        byte[] publicEncodedKey = Base64.decode(sharedPreferences.getString("publicKey", ""), 0);

        return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(publicEncodedKey));
    }

    public static PrivateKey getPrivateKey(Context context) throws
            InvalidKeySpecException, NoSuchAlgorithmException {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        byte[] privateEncodedKey = Base64.decode(sharedPreferences.getString("privateKey", ""), 0);

        return KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateEncodedKey));
    }


    private static X509Certificate generateCertificate(String cn, PublicKey
            publicKey, PrivateKey privateKey, String sigAlgID) {
        try {
            X500NameBuilder nameBuilder = new X500NameBuilder(BCStyle.INSTANCE);
            nameBuilder.addRDN(BCStyle.CN, cn);
            nameBuilder.addRDN(BCStyle.OU, "Phone Link");
            nameBuilder.addRDN(BCStyle.O, "Amadeo");

            Calendar calendar = Calendar.getInstance();
            Date from = calendar.getTime();
            calendar.add(Calendar.YEAR, 5);
            Date to = calendar.getTime();

            X509v3CertificateBuilder certificateBuilder = new JcaX509v3CertificateBuilder(nameBuilder.build(), BigInteger.ONE, from, to, nameBuilder.build(), publicKey);

            ContentSigner contentSigner = new JcaContentSignerBuilder(sigAlgID).setProvider("BC").build(privateKey);

            return new JcaX509CertificateConverter().setProvider("BC").getCertificate(certificateBuilder.build(contentSigner));
        } catch (OperatorCreationException | CertificateException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static SSLContext getSSLContext(Context context, DeviceBase device) {
        try {
            PrivateKey privateKey = getPrivateKey(context);

            char[] keyStorePass = "".toCharArray();

            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null, null);
            keyStore.setKeyEntry("privateKey", privateKey, keyStorePass, new Certificate[]{deviceCertificate});

            TrustManager[] trustManagers;
            if (device != null && !device.getDeviceId().equals("debugID")) {
                X509CertificateHolder certificateHolder = new X509CertificateHolder(device.getCertificate());
                X509Certificate remoteDeviceCertificate = new JcaX509CertificateConverter().setProvider(bouncyCastleProvider).getCertificate(certificateHolder);
                keyStore.setCertificateEntry(device.getDeviceId(), remoteDeviceCertificate);

                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                trustManagerFactory.init(keyStore);
                trustManagers = trustManagerFactory.getTrustManagers();
            } else {
                PhoneLinkUtils.logI(TAG, "insecure SSLContext generated!");

                trustManagers = new TrustManager[]{new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {

                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                }};
            }

            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(keyStore, keyStorePass);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(keyManagerFactory.getKeyManagers(), trustManagers, new SecureRandom());

            return sslContext;
        } catch (KeyStoreException | CertificateException | IOException | InvalidKeySpecException | KeyManagementException | UnrecoverableKeyException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static X509Certificate getDeviceCertificate() {
        return deviceCertificate;
    }

    public static synchronized String generateRandomString(int min, int max) {
        int stringLength = random.nextInt((max - min) + 1) + min;
        PhoneLinkUtils.logI(TAG, "Generating string with %d chars", stringLength);

        StringBuilder password = new StringBuilder();

        for (int i = 0; i < stringLength; i++)
            password.append(GEN_CHARACTERS.charAt(random.nextInt(GEN_CHARACTERS.length() - 1)));

        return password.toString();
    }
}


package amadeo.phonelink.device.network;

import android.content.Context;

import amadeo.phonelink.device.DeviceBase;
import amadeo.phonelink.device.DeviceManager;
import amadeo.phonelink.service.network.NetPackage;

/**
 * Created by Amadeo on 2018-07-06.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class DeviceManagerHelper {
    public static void sendNetPackageToAll(Context context, NetPackage netPackage) {
        for (DeviceBase c : DeviceManager.getInstance(context).getPairedDevices()) {
            if (DeviceManager.getInstance(context).onTrustedWiFi(c))
                c.getConnectionServer().sendNetPackage(netPackage);
        }
    }
}

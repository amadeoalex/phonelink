package amadeo.phonelink.device.network;

import android.content.Context;

import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocket;

import amadeo.phonelink.service.network.NetPackage;

/**
 * Created by Amadeo on 2018-05-27.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public interface ConnectionCallback {
    Context getContext();

    void startListener();

    void onConnected(SSLSocket newSocket) throws SSLHandshakeException;
    void onDisconnected();

    void onNetPackageReceived(NetPackage netPackage);

    void onConnectionBroken();
    void sendPackageConfirmation(NetPackage netPackage);
}

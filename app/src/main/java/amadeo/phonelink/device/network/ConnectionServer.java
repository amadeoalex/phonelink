package amadeo.phonelink.device.network;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StreamCorruptedException;
import java.lang.ref.WeakReference;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.LinkedBlockingDeque;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.SecurityUtils;
import amadeo.phonelink.device.DeviceBase;
import amadeo.phonelink.features.FeatureManager;
import amadeo.phonelink.service.network.NetPackage;
import amadeo.phonelink.ui.drawer.main.MainFragment;

/**
 * Created by Amadeo on 2018-07-05.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class ConnectionServer extends Thread implements ConnectionCallback {
    private final String TAG = "PhoneLink ConnectionServer";

    public static final String TYPE_CONFIRMATION = "net_package_received";
    public static final String KEY_ID = "net_package_id";

    private final WeakReference<Context> contextWeakReference;
    private final DeviceBase device;

    private volatile SSLSocket sslSocket;
    private volatile BufferedReader bufferedReader;
    private volatile PrintWriter printWriter;

    private ConnectionListener connectionListener;
    private PackageListener packageListener;

    private ConnectionWatchdog connectionWatchdog;

    private final LinkedBlockingDeque<NetPackage> netPackages = new LinkedBlockingDeque<>();

    private volatile boolean running = true;

    public ConnectionServer(Context context, DeviceBase computer) {
        this.contextWeakReference = new WeakReference<>(context);
        this.device = computer;

        connectionWatchdog = new ConnectionWatchdog(this);
    }

    @Override
    public Context getContext() {
        return contextWeakReference.get();
    }

    @Override
    public void onConnected(SSLSocket newSocket) throws SSLHandshakeException {
        sslSocket = newSocket;

        if (sslSocket != null) {
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(sslSocket.getInputStream(), "UTF-8"));
                printWriter = new PrintWriter(new OutputStreamWriter(sslSocket.getOutputStream(), "UTF-8"), true);

                if (packageListener != null && packageListener.isAlive()) {
                    try {
                        throw new Exception("packageListener should be null or not alive!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                packageListener = new PackageListener(this, bufferedReader);
                packageListener.start();

                sendConnectionChangeBroadcast();
                FeatureManager.onConnected();
            } catch (SSLHandshakeException e) {
                PhoneLinkUtils.logI(TAG, "ssl handshake exception, pair link is no longer valid?");
                PhoneLinkUtils.logI(TAG, e.getMessage());
                throw e;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDisconnected() {
        startListener();
        sendConnectionChangeBroadcast();
    }

    @Override
    public void onNetPackageReceived(NetPackage netPackage) {
        connectionWatchdog.received(netPackage);

        //send package confirmation
        if (!netPackage.type().equals(ConnectionServer.TYPE_CONFIRMATION))
            sendPackageConfirmation(netPackage);

        FeatureManager.parseNetPackage(netPackage);
    }

    @Override
    public void onConnectionBroken() {
        disconnect();
        onDisconnected();
    }

    @Override
    public void sendPackageConfirmation(NetPackage netPackage) {
        NetPackage confirmationPackage = new NetPackage(TYPE_CONFIRMATION);
        int packageId = netPackage.getInt(KEY_ID);
        confirmationPackage.setInt(KEY_ID, packageId);

        sendNetPackage(confirmationPackage);
    }

    private void sendConnectionChangeBroadcast() {
        Intent intent = new Intent(MainFragment.INTENT_CONNECTION_CHANGE);
        intent.putExtra(DeviceBase.KEY_DEVICE_ID, device.getDeviceId());
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }

    @Override
    public void startListener() {
        try {
            if (sslSocket != null) {
                if (!sslSocket.isClosed())
                    sslSocket.close();

                sslSocket = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//        PhoneLinkUtils.logI(TAG, "l1");
        if (connectionListener != null && connectionListener.isAlive()) {
            try {
                throw new Exception("startListener() should not be called when old one is still running!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (running) {
//            PhoneLinkUtils.logI(TAG, "l2");
            connectionListener = new ConnectionListener(contextWeakReference.get(), device, this, sslSocket);
            connectionListener.start();
        } else
            PhoneLinkUtils.logW(TAG, "thread is not running, ConnectionListener not created!");

//        PhoneLinkUtils.logI(TAG, "l3");
    }

    @Override
    public void run() {
        startListener();

        while (running) {
            NetPackage netPackage = null;

            try {
                PhoneLinkUtils.logI(TAG, "taking package");
                netPackage = netPackages.take();

                if (sslSocket == null) {
                    //TODO: restore checkip functionality
//                    if (!computer.checkIp())
//                        throw new ConnectException("checkIp");

                    PhoneLinkUtils.logI(TAG, "socket is null, trying to connect to %s at %d", device.getAddress(), device.getListeningTcpPort());
                    SSLContext sslContext = SecurityUtils.getSSLContext(contextWeakReference.get(), device);
                    if (sslContext == null)
                        throw new NullPointerException("sslSocket is null!");

                    SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
                    SSLSocket newSocket = (SSLSocket) sslSocketFactory.createSocket();
                    PhoneLinkUtils.logI(TAG, "connecting to %s at %d", device.getAddress(), device.getListeningTcpPort());
                    newSocket.connect(new InetSocketAddress(device.getAddress(), device.getListeningTcpPort()), 1000);

                    PhoneLinkUtils.logI(TAG, "interrupt and onconnect");
                    connectionListener.interrupt();
                    onConnected(newSocket);
                }

                if (netPackage.getInt(KEY_ID) == 0)
                    netPackage.setInt(KEY_ID, (int) System.currentTimeMillis());

                PhoneLinkUtils.logI(TAG, "send through socket");
                printWriter.println(netPackage.toString());

                if (!netPackage.type().equals(TYPE_CONFIRMATION))
                    connectionWatchdog.sent(netPackage);
            } catch (InterruptedException e) {
                PhoneLinkUtils.logI(TAG, "take() interrupted, killing thread");
                break;
            } catch (ConnectException e) {
                PhoneLinkUtils.logI(TAG, "ConnectException: %s, ignoring netPackage: %s", e.getMessage(), netPackage.type());
                FeatureManager.failedNetPackage(netPackage);
            } catch (SocketTimeoutException e) {
                PhoneLinkUtils.logI(TAG, "socket timeout exception, ignoring netPackage: %s", netPackage.toString());
                FeatureManager.failedNetPackage(netPackage);
            } catch (StreamCorruptedException e) {
                PhoneLinkUtils.logI(TAG, "stream is corrupted, closing socket and trying again: %s", e.getMessage());
                try {
                    sslSocket.close();
                    sslSocket = null;
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                netPackages.addFirst(netPackage);
            } catch (SocketException e) {
                PhoneLinkUtils.logI(TAG, "socket exception: %s, closing socket and trying again", e.getMessage());

                try {
                    sslSocket.close();
                    sslSocket = null;
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                netPackages.addFirst(netPackage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            if (connectionListener != null && connectionListener.isAlive()) {
                PhoneLinkUtils.logI(TAG, "joining ConnectionListener");
                connectionListener.interrupt();
                connectionListener.join();
                PhoneLinkUtils.logI(TAG, "joined ConnectionListener");
            }

            if (packageListener != null && packageListener.isAlive()) {
                PhoneLinkUtils.logI(TAG, "joining PackageListener");
                packageListener.interrupt();
                packageListener.join();
                PhoneLinkUtils.logI(TAG, "joined ConnectionListener");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        PhoneLinkUtils.logI(TAG, "thread finished");
    }

    public void sendNetPackage(NetPackage netPackage) {
        netPackages.add(netPackage);
    }

    public void sendPriorityNetPackage(NetPackage netPackage) {
        netPackages.addFirst(netPackage);
    }

    public boolean isConnected() {
        return sslSocket != null && sslSocket.isConnected();
    }

    public boolean disconnect() {
        if (isConnected()) {
            try {
                if (sslSocket != null && !sslSocket.isClosed()) {
                    sslSocket.close();
                    sslSocket = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        } else
            return false;
    }

    @Override
    public void interrupt() {
        running = false;
        super.interrupt();
        disconnect();
    }
}

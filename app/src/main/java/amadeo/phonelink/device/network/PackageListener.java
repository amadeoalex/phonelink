package amadeo.phonelink.device.network;

import java.io.BufferedReader;
import java.io.IOException;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.device.network.ConnectionCallback;
import amadeo.phonelink.features.FeatureManager;
import amadeo.phonelink.service.network.NetPackage;

/**
 * Created by Amadeo on 2018-05-27.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class PackageListener extends Thread {
    private final String TAG = "PhoneLink PL";

    private final ConnectionCallback connectionCallback;
    private final BufferedReader bufferedReader;

    private volatile boolean running = true;

    public PackageListener(ConnectionCallback connectionCallback, BufferedReader bufferedReader) {
        this.connectionCallback = connectionCallback;
        this.bufferedReader = bufferedReader;

    }

    @Override
    public void run() {
        PhoneLinkUtils.logI(TAG, "started");

        while (running) {
            try {
                String jsonString = bufferedReader.readLine();
                if (jsonString == null)
                    throw new IOException("json is null");

                NetPackage netPackage = NetPackage.deserialize(jsonString);
                if (netPackage == null) {
                    PhoneLinkUtils.logI(TAG, "error deserializing NetPackage!");
                    continue;
                }
                PhoneLinkUtils.logI(TAG, "package: %s", netPackage.type());

                connectionCallback.onNetPackageReceived(netPackage);
            } catch (IOException e) {
                PhoneLinkUtils.logI(TAG, "connection problem, resetting: %s", e.getMessage());
                Thread.currentThread().interrupt();

                connectionCallback.onDisconnected();
            }
        }

        PhoneLinkUtils.logI(TAG, "thread finished");
    }

    @Override
    public void interrupt() {
        PhoneLinkUtils.logI(TAG, "interrupted");
        running = false;

        super.interrupt();
    }
}

package amadeo.phonelink.device.network;

import android.content.Context;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSocket;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.SecurityUtils;
import amadeo.phonelink.device.DeviceBase;

/**
 * Created by Amadeo on 2018-05-27.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class ConnectionListener extends Thread {
    private final String TAG = "PhoneLink CL";

    private final ConnectionCallback connectionCallback;

    private SSLServerSocket sslServerSocket;

    private volatile boolean running = true;

    public ConnectionListener(Context context, DeviceBase device, ConnectionCallback connectionCallback, Socket oldSocket) {
        this.connectionCallback = connectionCallback;

        try {
            if (oldSocket != null) {
                oldSocket.close();
            }

            SSLContext sslContext = SecurityUtils.getSSLContext(context, device);
            if (sslContext == null)
                throw new NullPointerException("sslContext is null!");

            sslServerSocket = (SSLServerSocket) sslContext.getServerSocketFactory().createServerSocket();
            sslServerSocket.setReuseAddress(true);
            sslServerSocket.setNeedClientAuth(true);
            sslServerSocket.bind(new InetSocketAddress(device.getTcpPort()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        PhoneLinkUtils.logI(TAG, "started at %d", sslServerSocket.getLocalPort());

        try {
            SSLSocket newSocket = (SSLSocket) sslServerSocket.accept();
            connectionCallback.onConnected(newSocket);
            sslServerSocket.close();
        } catch (IOException e) {
            if (running)
                e.printStackTrace();
        }

        PhoneLinkUtils.logI(TAG, "thread finished");
    }

    @Override
    public void interrupt() {
        running = false;

        try {
            if (!sslServerSocket.isClosed())
                sslServerSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package amadeo.phonelink.device.network;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.service.network.NetPackage;

public class ConnectionWatchdog {
    private final String TAG = "PhoneLink CW";

    private int requiredId;
    private ArrayList<Integer> receivedNetPackages = new ArrayList<>();

    private ConnectionCallback connectionCallback;

    private final Timer timer;
    private TimerTask timerTask;
    private int interval = 5000;

    public ConnectionWatchdog(ConnectionCallback connectionCallback) {
        this.connectionCallback = connectionCallback;

        timer = new Timer();
    }

    public void check() {
        PhoneLinkUtils.logI(TAG, "check");
        if (!receivedNetPackages.contains(requiredId)) {
            PhoneLinkUtils.logW(TAG, "check failed");
            PhoneLinkUtils.logW(TAG, "required %d, registered %s", requiredId, receivedNetPackages);
            connectionCallback.onConnectionBroken();
        }
        receivedNetPackages.clear();
    }

    public void sent(NetPackage netPackage) {
        if (timerTask != null)
            timerTask.cancel();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                check();
            }
        };

        timer.schedule(timerTask, interval);

        requiredId = netPackage.getInt(ConnectionServer.KEY_ID);
    }

    public void received(NetPackage netPackage) {
        receivedNetPackages.add(netPackage.getInt(ConnectionServer.KEY_ID));
    }

}

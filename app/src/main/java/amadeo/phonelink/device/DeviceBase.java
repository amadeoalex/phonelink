package amadeo.phonelink.device;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.device.network.ConnectionServer;
import amadeo.phonelink.service.network.NetPackage;

public abstract class DeviceBase {
    private static final String TAG = "Device Base";

    //region Constants
    public static final String KEY_DEVICE_ID = "deviceID";
    public static final String KEY_CERTIFICATE = "certificate";
    public static final String KEY_INET_ADDRESS = "inetAddress";
    public static final String KEY_TCP_PORT = "tcpPort";
    public static final String KEY_LISTENING_TCP_PORT = "listeningTCPPort";
    public static final String KEY_BSSID = "bSSID";
    public static final String KEY_SSID = "SSID";
    public static final String KEY_TYPE = "deviceType";

    public static final String TYPE_DISCONNECT = "amadeo.phonelink.package.DISCONNECT";

    public static final String TYPE_PHONE = "phone";
    public static final String TYPE_TABLET = "tablet";
    public static final String TYPE_PC = "personalComputer";
    public static final String TYPE_LAPTOP = "laptop";
    //endregion

    //region Protected fields
    protected String deviceId;
    protected String type;
    protected byte[] certificate;
    protected String address;
    protected int tcpPort;
    protected int listeningTcpPort;
    protected String SSID;
    protected String bSSID;
    protected ConnectionServer connectionServer;
    //endregion

    //region Getters
    public String getDeviceId() {
        return deviceId;
    }

    public String getType() {
        return type;
    }

    public byte[] getCertificate() {
        return certificate;
    }

    public String getAddress() {
        return address;
    }

    public int getTcpPort() {
        return tcpPort;
    }

    public int getListeningTcpPort() {
        return listeningTcpPort;
    }

    public String getSSID() {
        return SSID;
    }

    public String getbSSID() {
        return bSSID;
    }

    public ConnectionServer getConnectionServer() {
        return connectionServer;
    }
    //endregion

    //region Setters
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCertificate(byte[] certificate) {
        this.certificate = certificate;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setTcpPort(int tcpPort) {
        this.tcpPort = tcpPort;
    }

    public void setListeningTcpPort(int listeningTcpPort) {
        this.listeningTcpPort = listeningTcpPort;
    }

    public void setSSID(String SSID) {
        this.SSID = SSID;
    }

    public void setbSSID(String bSSID) {
        this.bSSID = bSSID;
    }

//        public void setConnectionServer(ConnectionServer connectionServer) {
//        this.connectionServer = connectionServer;
//    }
    //endregion

    //region Methods
    public abstract void startServer(Context context);

    public abstract void stopServer();

    public abstract void joinServer();

    public abstract void unPair();

    public abstract void sendDisconnectRequest();

    public static DeviceBase fromIdNetPackage(NetPackage idNetPackage) {
        if (idNetPackage == null) {
            Log.i(TAG, "id net package is null");
            return null;
        }

        switch (idNetPackage.getString(KEY_TYPE)) {
            case TYPE_PHONE:
                return new Computer(idNetPackage.getString(KEY_DEVICE_ID), idNetPackage.getString(KEY_CERTIFICATE).getBytes(), idNetPackage.getString(KEY_INET_ADDRESS), idNetPackage.getInt(KEY_TCP_PORT), idNetPackage.getInt(KEY_LISTENING_TCP_PORT), idNetPackage.getString(KEY_SSID), idNetPackage.getString(KEY_BSSID));

            default:
                return null;
        }
    }

    public static DeviceBase deserialize(String jsonString) throws JSONException {
        JSONObject deviceJson = new JSONObject(jsonString);

        String deviceType = deviceJson.optString(KEY_TYPE, "MISSING_TYPE");
        switch (deviceType) {
            case TYPE_PC:
                return new Computer(deviceJson.getString(KEY_DEVICE_ID), android.util.Base64.decode(deviceJson.getString(KEY_CERTIFICATE), android.util.Base64.DEFAULT), deviceJson.getString(KEY_INET_ADDRESS), deviceJson.getInt(KEY_TCP_PORT), deviceJson.getInt(KEY_LISTENING_TCP_PORT), deviceJson.getString(KEY_SSID), deviceJson.getString(KEY_BSSID));

            default:
                throw new JSONException("unknown device type " + deviceType);
        }
    }
    //endregion
}

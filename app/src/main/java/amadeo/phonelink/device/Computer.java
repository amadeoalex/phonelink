package amadeo.phonelink.device;

import android.content.Context;
import android.os.Build;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.device.network.ConnectionServer;
import amadeo.phonelink.service.network.NetPackage;

public class Computer extends DeviceBase {
    private final String TAG = "Computer";

    public Computer(String id, byte[] certificate, String address, int tcpPort, int listeningTcpPort, String SSID, String bSSID) {
        this.deviceId = id;
        this.type = DeviceBase.TYPE_PC;
        this.certificate = certificate;
        this.address = address;
        this.tcpPort = tcpPort;
        this.listeningTcpPort = listeningTcpPort;
        this.SSID = SSID;
        this.bSSID = bSSID;
    }

    @Override
    public void startServer(Context context) {
        PhoneLinkUtils.logI(TAG, "starting server for %s", getDeviceId());
        if (connectionServer != null && !connectionServer.isAlive())
            connectionServer = null;

        if (connectionServer == null) {
            connectionServer = new ConnectionServer(context, this);
            connectionServer.start();
        } else {
            PhoneLinkUtils.logI(TAG, "server is already running");
        }
    }

    @Override
    public void stopServer() {
        if (connectionServer != null && connectionServer.isAlive() && !connectionServer.isInterrupted())
            connectionServer.interrupt();
        else
            PhoneLinkUtils.logI(TAG, "thread is null, not alive or already interrupted");
    }

    @Override
    public void joinServer() {
        try {
            stopServer();
            connectionServer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unPair() {
        //TODO: add feedback/netpackage with unpair information
        stopServer();
    }

    @Override
    public void sendDisconnectRequest() {
        try {
            DatagramSocket datagramSocket = new DatagramSocket();
            NetPackage netPackage = new NetPackage(TYPE_DISCONNECT);
            netPackage.setString(KEY_DEVICE_ID, Build.MODEL);

            byte[] bytes = netPackage.toString().getBytes();
            DatagramPacket datagramPacket = new DatagramPacket(bytes, bytes.length, InetAddress.getByName(address), 4848);
            datagramSocket.send(datagramPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        JSONObject deviceJson = new JSONObject();
        try {
            deviceJson.put(DeviceBase.KEY_DEVICE_ID, deviceId);
            deviceJson.put(DeviceBase.KEY_TYPE, type);
            deviceJson.put(DeviceBase.KEY_CERTIFICATE, android.util.Base64.encodeToString(certificate, android.util.Base64.DEFAULT));
            deviceJson.put(DeviceBase.KEY_INET_ADDRESS, address);
            deviceJson.put(DeviceBase.KEY_TCP_PORT, tcpPort);
            deviceJson.put(DeviceBase.KEY_LISTENING_TCP_PORT, listeningTcpPort);
            deviceJson.put(DeviceBase.KEY_SSID, SSID);
            deviceJson.put(DeviceBase.KEY_BSSID, bSSID == null ? JSONObject.NULL : bSSID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return deviceJson.toString();
    }
}

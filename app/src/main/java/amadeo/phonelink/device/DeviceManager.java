package amadeo.phonelink.device;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.util.ArrayList;

import amadeo.phonelink.NetworkUtils;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.service.network.ConnectionChangeCallback;

/**
 * Created by Amadeo on 2017-12-23.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class DeviceManager {
    private final String TAG = "PhoneLink DM";

    private final String PAIRED_DEVICES = "paired_computer";
    private final WeakReference<Context> contextWeakReference;

    private ArrayList<DeviceBase> pairedDevices;

    private final SharedPreferences sharedPreferences;

    private WifiInfo currentWiFiConnection;

    private static volatile DeviceManager instance;

    private DeviceManager(Context context) {
        this.contextWeakReference = new WeakReference<>(context.getApplicationContext());

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        currentWiFiConnection = NetworkUtils.getWiFiInfo(contextWeakReference.get());
        deserializeDevices();
    }

    public static DeviceManager getInstance(Context context) {
        if (instance == null) {
            synchronized (DeviceManager.class) {
                if (instance == null)
                    instance = new DeviceManager(context);
            }
        }

        return instance;
    }

    private void deserializeDevices() {
        pairedDevices = new ArrayList<>();

        try {
            JSONArray serializedDevices = new JSONArray(sharedPreferences.getString(PAIRED_DEVICES, "[]"));
            for (int i = 0; i < serializedDevices.length(); i++) {
                DeviceBase device = null;
                try {
                    device = DeviceBase.deserialize(serializedDevices.getString(i));
                } catch (JSONException e) {
                    PhoneLinkUtils.logW(TAG, "JSONException while deserializing device '%s':\n %s", e.getMessage(), serializedDevices.getString(i));
                    continue;
                }

                pairedDevices.add(device);
            }
        } catch (JSONException e) {
            PhoneLinkUtils.logW(TAG, "JSON exception while deserializing devices: %s", e.getMessage());
        }
    }

    public boolean isPaired(DeviceBase device) {
        return pairedDevices.contains(device);
    }

    public boolean isPaired(InetAddress inetAddress) {
        for (DeviceBase computer : pairedDevices) {
            if (computer.getAddress().equals(inetAddress.getHostAddress()))
                return true;
        }

        return false;
    }

    public boolean addDevice(DeviceBase device) {
        if (!isPaired(device)) {
            pairedDevices.add(device);
            PhoneLinkUtils.logI(TAG, "%s added to paired devices", device.getDeviceId());
            savePreferences();

            return true;
        } else {
            PhoneLinkUtils.logI(TAG, "%s is already paired", device.getDeviceId());

            return false;
        }
    }

    public boolean removePairedDevice(DeviceBase device) {
        if (isPaired(device)) {
            device.unPair();
            pairedDevices.remove(device);
            savePreferences();

            return true;
        } else {
            PhoneLinkUtils.logI(TAG, "[C]no device with id %s paired", device.getDeviceId());

            return false;
        }
    }

    public boolean removePairedDevice(String deviceID) {
        DeviceBase device = getPairedDevice(deviceID);
        if (device != null) {
            removePairedDevice(device);

            return true;
        }

        return false;
    }

    public DeviceBase getPairedDevice(String deviceID) {
        for (DeviceBase c : pairedDevices)
            if (c.getDeviceId().equals(deviceID))
                return c;

        PhoneLinkUtils.logI(TAG, "[ID]no device with id %s", deviceID);
        return null;
    }

    public DeviceBase getPairedDevice(int index) {
        if (pairedDevices.size() > 0 && pairedDevices.size() >= index)
            return pairedDevices.get(index);

        PhoneLinkUtils.logI(TAG, "no device with index %d", index);
        return null;
    }

    public boolean checkPortAvailability(int port) {
        for (DeviceBase c : pairedDevices) {
            if (c.getTcpPort() == port || c.getListeningTcpPort() == port)
                return false;
        }

        return true;
    }

    public ArrayList<DeviceBase> getPairedDevices() {
        return pairedDevices;
    }

    public boolean onTrustedWiFi(DeviceBase device) {
        //TODO: remove before "release"
        if (PhoneLinkUtils.inEmulator()) {
            PhoneLinkUtils.logI(TAG, "trust check true - in emulator");
            return true;
        }

        if (currentWiFiConnection == null || device == null || currentWiFiConnection.getBSSID() == null || currentWiFiConnection.getSSID() == null)
            return false;

        //PhoneLinkUtils.logI(TAG, "KEY_BSSID: %s|%s, SSID: %s|%s", currentWiFiConnection.getBSSID(), computer.getBSSID(), currentWiFiConnection.getSSID(), computer.getSSID());
        if (currentWiFiConnection.getBSSID().equals(device.getbSSID()) && currentWiFiConnection.getSSID().equals(device.getSSID())) {
            return true;
        } else {
            PhoneLinkUtils.logI(TAG, "trust check fail: TS: %s, CS: %s || TB: %s, CB: %s", device.getSSID(), currentWiFiConnection.getSSID(), device.getbSSID(), currentWiFiConnection.getSSID());
            return false;
        }
    }

    public boolean onAnyTrustedWiFi() {
        for (DeviceBase device : pairedDevices) {
            if (onTrustedWiFi(device))
                return true;
        }

        return false;
    }

    private void savePreferences() {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        JSONArray jsonArray = new JSONArray();
        for (DeviceBase computer : pairedDevices)
            jsonArray.put(computer.toString());

        editor.putString(PAIRED_DEVICES, jsonArray.toString());
        editor.apply();
    }

    public void onConnected(WifiInfo wifiInfo) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (DeviceBase c : pairedDevices) {
                    c.sendDisconnectRequest();
                }
            }
        }).start();

        currentWiFiConnection = wifiInfo;
    }

    public void onDisconnected() {

    }

    public void initDevices() {
        for (DeviceBase device : pairedDevices)
            initDevice(device);
    }

    public void initDevice(DeviceBase device) {
        if (device != null)
            device.startServer(contextWeakReference.get());
    }

    public void onDestroy() {
        for (DeviceBase c : pairedDevices) {
            c.joinServer();
        }
    }
}

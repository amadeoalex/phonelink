package amadeo.phonelink.features.smsnotifications;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import amadeo.phonelink.PreferenceHelpers;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.device.network.DeviceManagerHelper;
import amadeo.phonelink.features.Feature;
import amadeo.phonelink.features.FeatureManager;
import amadeo.phonelink.service.NotificationListener;
import amadeo.phonelink.service.network.NetPackage;

/**
 * Created by Amadeo on 2017-12-09.
 */

public class SMSNotifications extends Feature implements SMSContentObserver.SentSMSCallback {
    private static final String TAG = "PhoneLink SMSNotif.";

    //region Constants
    public static final String NAME = "SMSNotification";
    public static final String DISPLAY_NAME = "SMS Notifications";
    public static final String PREFIX = "telephony_";

    public static final String KEY_DATE = "date";
    public static final String KEY_BODY = "body";
    public static final String KEY_NUMBER = "number";
    public static final String KEY_NAME = "name";
    public static final String KEY_NUMBERS = "numbers";
    public static final String KEY_LAST_MESSAGES = "last_messages";
    public static final String KEY_MESSAGES = "messages";
    public static final String KEY_NUMBER_NAMES = "number_names";
    public static final String TYPE_SMS = "telephony_sms";
    public static final String TYPE_SMS_SEND = "telephony_send_sms";
    public static final String TYPE_SMS_SENT = "telephony_sent_sms";
    public static final String TYPE_SMS_READ = "telephony_read_sms";
    public static final String TYPE_SMS_DENIED = "telephony_denied_sms";
    public static final String TYPE_CONVERSATION_SYNC = "telephony_conversation_sync";
    public static final String TYPE_CONVERSATION_SYNC_DATA = "telephony_conversation_sync_data";
    public static final String TYPE_CONVERSATION_SYNC_ALL = "telephony_conversation_sync_all";
    public static final String TYPE_CONVERSATION_NAME_FROM_NUMBER = "telephony_conversation_name_from_number";
    public static final String TYPE_CONVERSATION_NAME_FROM_NUMBER_DATA = "telephony_conversation_name_from_number_data";

    public static final String TYPE_UPDATE_TIMESTAMP = "telephony_update_time_stamp";
    //endregion

    private BroadcastReceiver broadcastReceiver;
    private IntentFilter filter;
    private SMSContentObserver smsContentObserver;

    public SMSNotifications(Context context) {
        super(context);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDisplayName() {
        return DISPLAY_NAME;
    }

    @Override
    public String getPrefix() {
        return PREFIX;
    }

    @Override
    public void onCreate() {
        PhoneLinkUtils.logI(TAG, "created!");

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (PhoneLinkUtils.inEmulator() || FeatureManager.isStarted()) {
                    Bundle bundle = intent.getExtras();

                    if (bundle != null) {
                        StringBuilder smsBody = new StringBuilder();

                        SmsMessage[] msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                        final SmsMessage sms = msgs[0];

                        for (SmsMessage s : msgs)
                            smsBody.append(s.getMessageBody());

                        NetPackage netPackage = new NetPackage(TYPE_SMS);
                        netPackage.setString(KEY_DATE, Long.toString(PhoneLinkUtils.removeHundreds(System.currentTimeMillis())));
                        netPackage.setString(KEY_BODY, smsBody.toString());
                        netPackage.setString(KEY_NUMBER, PhoneLinkUtils.stripNumber(sms.getOriginatingAddress()));
                        String name = ContactsManager.nameByNumber(context, sms.getOriginatingAddress());
                        if (name != null) {
                            netPackage.setString(KEY_NAME, name);
                        }

                        PhoneLinkUtils.logI(TAG, "sms");
                        DeviceManagerHelper.sendNetPackageToAll(getContext(), netPackage);
                    } else
                        PhoneLinkUtils.logI(TAG, "bundle is null!");
                } else {
                    PhoneLinkUtils.logI(TAG, "broadcast ignored");
                }
            }
        };

        filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        filter.setPriority(999);

        smsContentObserver = new SMSContentObserver(getContext(), this);
    }

    @Override
    public void onStart() {
        getContext().registerReceiver(broadcastReceiver, filter);
        getContext().getContentResolver().registerContentObserver(Uri.parse(ContactsManager.URI_CONTENT_SMS), true, smsContentObserver);
    }

    @Override
    public void onConnected() {

    }

    @Override
    public void onStop() {
        try {
            getContext().unregisterReceiver(broadcastReceiver);
        } catch (IllegalArgumentException e) {
            PhoneLinkUtils.logI(TAG, "sms receiver not registered");
        }
        getContext().getContentResolver().unregisterContentObserver(smsContentObserver);
    }

    @Override
    public void onNetPackageReceived(NetPackage netPackage) {
        final NetPackage returnPackage;

        String type = netPackage.type();
        try {
            switch (type) {
                case TYPE_CONVERSATION_SYNC:
                    if (PreferenceHelpers.optBoolean(getPreferences(), "pref_key_smsagent_conversation_sync", true)) {
                        returnPackage = syncPackage(getContext(), netPackage);

                        DeviceManagerHelper.sendNetPackageToAll(getContext(), returnPackage);
                    }
                    break;
                case TYPE_CONVERSATION_SYNC_ALL:
                    if (PreferenceHelpers.optBoolean(getPreferences(), "pref_key_smsagent_conversation_sync", true)) {
                        returnPackage = syncAllPackage(getContext(), netPackage);

                        DeviceManagerHelper.sendNetPackageToAll(getContext(), returnPackage);
                    }
                    break;
                case TYPE_CONVERSATION_NAME_FROM_NUMBER:
                    if (PreferenceHelpers.optBoolean(getPreferences(), "pref_key_smsagent_number_sync", true)) {
                        returnPackage = nameFromNumberPackage(getContext(), netPackage);

                        DeviceManagerHelper.sendNetPackageToAll(getContext(), returnPackage);
                    }
                    break;
                case TYPE_SMS_SEND:
                    if (PreferenceHelpers.optBoolean(getPreferences(), "pref_key_smsagent_send_sms", false)) {
                        PhoneLinkUtils.logI(TAG, "N: " + netPackage.getString(KEY_NUMBER) + " B: " + netPackage.getString(KEY_BODY));

                        smsContentObserver.setMessageToIgnore(netPackage.getString(KEY_NUMBER), netPackage.getString(KEY_BODY));

                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(netPackage.getString(KEY_NUMBER), null, netPackage.getString(KEY_BODY), null, null);
                    } else {
                        PhoneLinkUtils.logI(TAG, "sms send request denied");
                        returnPackage = new NetPackage(TYPE_SMS_DENIED);
                        returnPackage.setString(KEY_NUMBER, netPackage.getString(KEY_NUMBER));
                        returnPackage.setString(KEY_BODY, netPackage.getString(KEY_BODY));
                        returnPackage.setString(KEY_DATE, netPackage.getString(KEY_DATE));

                        //service.deviceManager.getPairedDevice(0).getConnectionServer().sendNetPackage(returnPackage);
                        DeviceManagerHelper.sendNetPackageToAll(getContext(), returnPackage);
                    }
                    break;
                case TYPE_SMS_READ:
                    PhoneLinkUtils.logI(TAG, "clearing sms notifications");
                    Intent intent = new Intent(NotificationListener.BROADCAST_ACTION);
                    intent.putExtra(NotificationListener.BROADCAST_ACTION, NotificationListener.COMMAND_REMOVE_SMS_NOTIFICATIONS);
                    intent.putExtra(Notification.EXTRA_TEXT, netPackage.getString(KEY_BODY));
                    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
                    break;


                case "blaah":
                    returnPackage = new NetPackage("naaah_blaah");

                    //service.deviceManager.getPairedDevice(0).getConnectionServer().sendNetPackage(returnPackage);
                    DeviceManagerHelper.sendNetPackageToAll(getContext(), returnPackage);

                    break;

                default:
                    PhoneLinkUtils.logI(TAG, "unknown package type -%s- received.", type);
                    break;
            }
        } catch (JSONException e) {
            PhoneLinkUtils.logW(TAG, "preferences json exception: %s", e.getMessage());
        }
    }

    @Override
    public void onNetPackageFailed(NetPackage netPackage) {

    }

    @Override
    public void onSentSMS(HashMap<String, String> messageMap) {
        PhoneLinkUtils.logI(TAG, "sent sms");

        //TODO: idea, is it necessary to send update package if I send the same data once again in sent_sms package? - goddamnnit it's too late x.x
        NetPackage updatePackage = new NetPackage(TYPE_UPDATE_TIMESTAMP);
        updatePackage.setString(KEY_NUMBER, messageMap.get(KEY_NUMBER));
        updatePackage.setString(KEY_BODY, messageMap.get(KEY_BODY));
        updatePackage.setString(KEY_DATE, messageMap.get(KEY_DATE));

        DeviceManagerHelper.sendNetPackageToAll(getContext(), updatePackage);

        NetPackage netPackage = new NetPackage(TYPE_SMS_SENT);
        netPackage.setString(KEY_DATE, messageMap.get(KEY_DATE));
        netPackage.setString(KEY_BODY, messageMap.get(KEY_BODY));
        netPackage.setString(KEY_NUMBER, messageMap.get(KEY_NUMBER));
        String name = ContactsManager.nameByNumber(getContext(), messageMap.get(KEY_NUMBER));
        if (name != null) {
            netPackage.setString(KEY_NAME, name);
        }

        DeviceManagerHelper.sendNetPackageToAll(getContext(), netPackage);
    }

    private NetPackage syncPackage(Context context, NetPackage netPackage) {
        NetPackage returnPackage = new NetPackage(TYPE_CONVERSATION_SYNC_DATA);

        try {
            JSONObject messages = new JSONObject();

            JSONArray numbers = netPackage.getJSONArray(KEY_NUMBERS);
            JSONObject lastMessages = netPackage.getJSONObject(KEY_LAST_MESSAGES);

            JSONArray returnNumbers = new JSONArray();
            for (int i = 0; i < numbers.length(); i++) {
                String number = numbers.getString(i);

                JSONArray foundMessages = ContactsManager.getConversationFrom(context, number, lastMessages.getJSONObject(number));
                if (foundMessages.length() > 0) {
                    returnNumbers.put(number);
                    messages.put(number, foundMessages);
                }

            }

            returnPackage.setJSONArray(KEY_NUMBERS, returnNumbers);
            returnPackage.setJSONObject(KEY_MESSAGES, messages);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return returnPackage;
    }

    private NetPackage syncAllPackage(Context context, NetPackage netPackage) {
        NetPackage returnPackage = new NetPackage(TYPE_CONVERSATION_SYNC_DATA);
        HashMap<String, JSONArray> conversations = ContactsManager.getAllConversations(context);
        try {
            JSONObject messages = new JSONObject();
            JSONArray numbers = new JSONArray();

            for (Map.Entry<String, JSONArray> entry : conversations.entrySet()) {
                String number = entry.getKey();
                JSONArray conversation = entry.getValue();

                numbers.put(number);

                messages.put(number, conversation);

            }

            returnPackage.setJSONArray(KEY_BODY, numbers);
            returnPackage.setJSONObject(KEY_MESSAGES, messages);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return returnPackage;
    }

    private NetPackage nameFromNumberPackage(Context context, NetPackage netPackage) {
        NetPackage returnPackage = new NetPackage(TYPE_CONVERSATION_NAME_FROM_NUMBER_DATA);

        JSONObject returnNumberNames = new JSONObject();


        JSONArray numbers = netPackage.getJSONArray(KEY_NUMBERS);
        try {
            for (int i = 0; i < numbers.length(); i++) {
                returnNumberNames.put(numbers.getString(i), ContactsManager.nameByNumber(context, numbers.getString(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        returnPackage.setJSONObject(KEY_NUMBER_NAMES, returnNumberNames);

        return returnPackage;
    }

    @Override
    public boolean hasSettings() {
        return true;
    }
}

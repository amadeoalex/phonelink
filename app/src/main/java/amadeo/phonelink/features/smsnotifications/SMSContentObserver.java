package amadeo.phonelink.features.smsnotifications;

import android.content.Context;
import android.database.ContentObserver;

import java.lang.ref.WeakReference;
import java.util.HashMap;

import amadeo.phonelink.PhoneLinkUtils;

/**
 * Created by Amadeo on 2018-03-23.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class SMSContentObserver extends ContentObserver {
    public interface SentSMSCallback {
        void onSentSMS(HashMap<String, String> messageMap);
    }

    private final WeakReference<Context> contextWeakReference;
    private final SentSMSCallback sentSMSCallback;

    private String lastId = "";

    private String ignoredMessageNumber = "";
    private String ignoredMessageBody = "";

    public SMSContentObserver(Context context, SentSMSCallback sentSMSCallback) {
        super(null);

        contextWeakReference = new WeakReference<>(context);
        this.sentSMSCallback = sentSMSCallback;
    }

    @Override
    public boolean deliverSelfNotifications() {
        return false;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);

        HashMap<String, String> lastSentMessageMap = ContactsManager.getLastSentMessageMap(contextWeakReference.get());
//        System.out.println(lastSentMessageMap);

        if (lastSentMessageMap.isEmpty())
            return;

        if (!lastSentMessageMap.get("id").equals(lastId)) {
            if (lastSentMessageMap.get(SMSNotifications.KEY_NUMBER).equals(ignoredMessageNumber) && lastSentMessageMap.get(SMSNotifications.KEY_BODY).equals(ignoredMessageBody)) {
                ignoredMessageNumber = "";
                ignoredMessageBody = "";
            } else {
                PhoneLinkUtils.logI("PhoneLink", lastSentMessageMap.toString());
                sentSMSCallback.onSentSMS(lastSentMessageMap);
                lastId = lastSentMessageMap.get("id");
            }
        }
    }

    public void setMessageToIgnore(String number, String body) {
        ignoredMessageNumber = number;
        ignoredMessageBody = body;
    }
}

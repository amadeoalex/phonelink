package amadeo.phonelink.features.smsnotifications;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import amadeo.phonelink.PhoneLinkUtils;

/**
 * Created by Amadeo on 2017-11-12.
 */
public class ContactsManager {
    private static final String TAG = "PhoneLink CM";

    private static final String COLUMN_NAME_BODY = "body";
    private static final String COLUMN_NAME_DATE = "date";
    private static final String COLUMN_NAME_TYPE = "type";
    private static final String COLUMN_NAME_ADDRESS = "address";
    private static final String COLUMN_NAME_ID = "_id";

    private static final String SMS_TYPE_IN = "1";
    private static final String SMS_TYPE_OUT = "2";

    public static final String URI_CONTENT_SMS = "content://sms";

    public static synchronized String nameByNumber(Context context, String number) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        Cursor cursor = context.getContentResolver().query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

        String name = number;
        if (cursor != null) {
            if (cursor.moveToFirst())
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));

            cursor.close();
        }

        return name;
    }

    public static synchronized HashMap<String, JSONArray> getAllConversations(Context context) {
        HashMap<String, JSONArray> conversations = new HashMap<>();

        Uri uri = Uri.parse(URI_CONTENT_SMS);
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, "date ASC");

        if (cursor != null) {
            int bodyIndex = cursor.getColumnIndex(COLUMN_NAME_BODY);
            int dateIndex = cursor.getColumnIndex(COLUMN_NAME_DATE);
            int typeIndex = cursor.getColumnIndex(COLUMN_NAME_TYPE);
            int addressIndex = cursor.getColumnIndex(COLUMN_NAME_ADDRESS);

            try {
                while (cursor.moveToNext()) {
                    String number = PhoneLinkUtils.stripNumber(cursor.getString(addressIndex));

                    JSONArray conversation = conversations.get(number);
                    if (conversation == null) {
                        conversations.put(number, new JSONArray());
                        conversation = conversations.get(number);
                    }

                    String body = cursor.getString(bodyIndex);
                    String date = Long.toString(cursor.getLong(dateIndex));
                    String type = cursor.getString(typeIndex);

                    conversation.put(new JSONObject().put(SMSNotifications.KEY_NAME, type.equals(SMS_TYPE_IN) ? number : null).put(SMSNotifications.KEY_BODY, body).put(SMSNotifications.KEY_DATE, date));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
        }

        return conversations;
    }

    public static synchronized JSONArray getConversationFrom(Context context, String number, JSONObject lastMessageMap) {
        PhoneLinkUtils.logI(TAG, "received last message: %s", lastMessageMap);

        JSONArray conversation = new JSONArray();

        number = PhoneLinkUtils.stripNumber(number);

        Uri uri = Uri.parse(URI_CONTENT_SMS);
        String condition = "address LIKE ?";
        String[] selection = new String[]{"%" + number + "%"};
        Cursor cursor = context.getContentResolver().query(uri, null, condition, selection, "date DESC");
//        Cursor cursor = context.getContentResolver().query(uri, null, "address LIKE '%" + number + "%'", null, "date DESC");

        if (cursor != null) {
            int bodyIndex = cursor.getColumnIndex(COLUMN_NAME_BODY);
            int dateIndex = cursor.getColumnIndex(COLUMN_NAME_DATE);
            int typeIndex = cursor.getColumnIndex(COLUMN_NAME_TYPE);

            try {
                long lastMessageDate = PhoneLinkUtils.removeHundreds(Long.parseLong(lastMessageMap.getString(SMSNotifications.KEY_DATE)));

                while (cursor.moveToNext()) {
                    long currentMessageDate = PhoneLinkUtils.removeHundreds(cursor.getLong(dateIndex));

                    String body = cursor.getString(bodyIndex);
                    String date = Long.toString(currentMessageDate);
                    String type = cursor.getString(typeIndex);

//                    PhoneLinkUtils.logI(TAG, "Body: %s, date: %s, type: %s, address: %s", body, date, type, cursor.getString(cursor.getColumnIndex("address")));

                    if (currentMessageDate < lastMessageDate) {
                        PhoneLinkUtils.logI(TAG, "no messages found");
                        conversation = new JSONArray();
                        break;
                    }

                    if (body.equals(lastMessageMap.get(SMSNotifications.KEY_BODY)) && currentMessageDate == lastMessageDate)
                        break;

                    conversation.put(new JSONObject().put(SMSNotifications.KEY_NAME, type.equals(SMS_TYPE_IN) ? number : null).put(SMSNotifications.KEY_BODY, body).put(SMSNotifications.KEY_DATE, date));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
        }

        return PhoneLinkUtils.reverseOrder(conversation);
    }

    public static synchronized HashMap<String, String> getLastSentMessageMap(Context context) {
        Uri uri = Uri.parse(URI_CONTENT_SMS);

        String selection = "type = ?";
        String[] selectionArgs = new String[]{SMS_TYPE_OUT};
        Cursor cursor = context.getContentResolver().query(uri, null, selection, selectionArgs, "date DESC");
        //Cursor cursor = context.getContentResolver().query(uri, null, null, null, "date DESC");

        HashMap<String, String> lastMessageMap = new HashMap<>();

        if (cursor != null) {
            cursor.moveToFirst();

            int bodyIndex = cursor.getColumnIndex(COLUMN_NAME_BODY);
            int dateIndex = cursor.getColumnIndex(COLUMN_NAME_DATE);
            int addressIndex = cursor.getColumnIndex(COLUMN_NAME_ADDRESS);
            int idIndex = cursor.getColumnIndex(COLUMN_NAME_ID);

            String body = cursor.getString(bodyIndex);
            String date = Long.toString(PhoneLinkUtils.removeHundreds(cursor.getLong(dateIndex)));
            String address = PhoneLinkUtils.stripNumber(cursor.getString(addressIndex));
            String id = cursor.getString(idIndex);

            lastMessageMap.put(SMSNotifications.KEY_BODY, body);
            lastMessageMap.put(SMSNotifications.KEY_DATE, date);
            lastMessageMap.put(SMSNotifications.KEY_NUMBER, address);
            lastMessageMap.put("id", id);

            cursor.close();
        }

        return lastMessageMap;
    }
}

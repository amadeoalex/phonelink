package amadeo.phonelink.features;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.PowerManager;
import android.support.v7.preference.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Set;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.service.network.NetPackage;

/**
 * Created by Amadeo on 2017-12-09.
 */

public class FeatureManager {
    private static final String TAG = "PhoneLink FM";

    private static final String DISABLED_PLUGINS = "feature_manager_disabled_plugins";

    private static final HashMap<String, Feature> features = new HashMap<>();
    private static WeakReference<Context> contextWeakReference;
    private static JSONObject disabledFeatures;

    private static PowerManager.WakeLock wakeLock;

    private static boolean started = true;

    public static synchronized void init(Context context) {
        FeatureManager.contextWeakReference = new WeakReference<>(context);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            disabledFeatures = new JSONObject(sharedPreferences.getString(DISABLED_PLUGINS, "{}"));
        } catch (JSONException e) {
            PhoneLinkUtils.logW(TAG, "exception parsing disabled plugins json");
        }

        PowerManager powerManager = (PowerManager) contextWeakReference.get().getSystemService(Context.POWER_SERVICE);
        if (powerManager != null)
            wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "phonelink:featuremanager");
    }

    public static synchronized void registerFeature(Feature feature) {
        features.put(feature.getName(), feature);
        feature.onCreate();

        if (disabledFeatures.optString(feature.getName(), null) == null) {
            feature.onStart();
        } else {
            PhoneLinkUtils.logI(TAG, "feature %s registered but disabled", feature.getName());
        }
    }

    public static synchronized void enableFeature(String name) {
        disabledFeatures.remove(name);
        savePreferences();
        getFeature(name).onStart();
    }

    public static synchronized void disableFeature(String name) {
        try {
            disabledFeatures.put(name, "");
            savePreferences();
            getFeature(name).onStop();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static synchronized boolean isEnabled(String name) {
        return !disabledFeatures.has(name);
    }

    public static boolean isStarted() {
        return started;
    }

    public static synchronized Feature getFeature(String name) {
        return features.get(name);
    }

    public static synchronized Set<String> getFeatures() {
        return features.keySet();
    }

    public static synchronized void onConnected() {
        for (Feature f : features.values())
            if (isEnabled(f.getName()))
                f.onConnected();
    }

    public static synchronized void stopFeatures() {
        if (started) {
            for (Feature f : features.values()) {
                f.onStop();
            }
            started = false;
        }
    }

    public static synchronized void startFeatures() {
        if (!started) {
            for (Feature f : features.values()) {
                f.onStart();
            }
            started = true;
        }
    }

    public static synchronized void parseNetPackage(NetPackage netPackage) {
        wakeLock.acquire(30000);
        try {
            for (Feature f : features.values()) {
                if (isEnabled(f.getName()) && netPackage.type().startsWith(f.getPrefix()))
                    f.onNetPackageReceived(netPackage);
            }
        } finally {
            if (wakeLock.isHeld())
                wakeLock.release();
        }
    }

    public static synchronized void failedNetPackage(NetPackage netPackage) {
        wakeLock.acquire(30000);
        try {
            for (Feature f : features.values()) {
                if (isEnabled(f.getName()) && netPackage.type().startsWith(f.getPrefix()))
                    f.onNetPackageFailed(netPackage);
            }
        } finally {
            if (wakeLock.isHeld())
                wakeLock.release();
        }
    }

    public static synchronized void onDestroy() {
        stopFeatures();

        PhoneLinkUtils.logI(TAG, "cleanup successful");
    }

    private static void savePreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(contextWeakReference.get());
        sharedPreferences.edit().putString(DISABLED_PLUGINS, disabledFeatures.toString()).apply();
        PhoneLinkUtils.logI(TAG, "saving?");
    }
}

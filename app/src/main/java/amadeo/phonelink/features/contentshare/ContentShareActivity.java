package amadeo.phonelink.features.contentshare;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.zip.DataFormatException;
import java.util.zip.GZIPInputStream;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.R;
import amadeo.phonelink.device.network.DeviceManagerHelper;
import amadeo.phonelink.features.FeatureManager;
import amadeo.phonelink.service.network.NetPackage;

public class ContentShareActivity extends AppCompatActivity {
    private static final String TAG = "PhoneLink LinkShareA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (action == null || type == null) {
            finish();
            return;
        }

        AlertDialog.Builder dialogBuilder = null;

        if (action.equals(Intent.ACTION_SEND)) {
            if (type.equals("text/plain")) {

                //TODO: add support for sending plain text - check if received text/plain is actually url
                dialogBuilder = new AlertDialog.Builder(this)
                        .setTitle(intent.getStringExtra(Intent.EXTRA_TEXT))
                        .setItems(R.array.contentshare_dialog_url_items, (dialog, which) -> {
                            NetPackage urlPackage = new NetPackage(ContentShare.TYPE_URL);
                            urlPackage.setString(ContentShare.KEY_TYPE_URL_URL, intent.getStringExtra(Intent.EXTRA_TEXT));
                            urlPackage.setString(ContentShare.KEY_TYPE_URL_DATE, Long.toString(System.currentTimeMillis()));

                            //TODO: add more options?
                            ContentShare contentShareFeature = (ContentShare) FeatureManager.getFeature(ContentShare.NAME);
                            contentShareFeature.share(urlPackage);
                            ContentShareActivity.this.finish();
                        });
            } else {
                Uri imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
                String fileName = PhoneLinkUtils.getFileNameFromURI(this, imageUri);

                if (imageUri != null && fileName != null) {
                    String packageType = type.startsWith("image/") ? ContentShare.TYPE_IMG : ContentShare.TYPE_FILE;
                    NetPackage filePackage = new NetPackage(packageType);

                    filePackage.setString(ContentShare.KEY_TYPE_FILE_NAME, fileName);

                    dialogBuilder = new AlertDialog.Builder(this)
                            .setTitle(fileName)
                            .setItems(R.array.contentshare_dialog_file_items, (dialog, which) -> {
                                String mode = which == 0 ? ContentShare.KEY_TYPE_FILE_MODE_SHARE : (which == 1 ? ContentShare.KEY_TYPE_FILE_MODE_SHARE_AND_CB : ContentShare.KEY_TYPE_FILE_MODE_CLIPBOARD);
                                filePackage.setString(ContentShare.KEY_TYPE_FILE_MODE, mode);

                                try (InputStream inputStream = getContentResolver().openInputStream(imageUri)) {
                                    if (inputStream != null) {

                                        String fileBase64String = Base64.encodeToString(PhoneLinkUtils.readAllBytes(inputStream), 0);
                                        filePackage.setString(ContentShare.KEY_TYPE_FILE_BASE64, fileBase64String);
                                        filePackage.setString(ContentShare.KEY_TYPE_FILE_DATE, Long.toString(System.currentTimeMillis()));

                                        DeviceManagerHelper.sendNetPackageToAll(this, filePackage);
                                    }

                                } catch (FileNotFoundException e) {
                                    PhoneLinkUtils.logI(TAG, "file not found: %s", imageUri);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                ContentShareActivity.this.finish();
                            });
                }
            }

            if (dialogBuilder == null) {
                finish();
                return;
            }

            AlertDialog dialog = dialogBuilder.create();
            dialog.setCanceledOnTouchOutside(true);
            dialog.setOnCancelListener(dialogInterface -> ContentShareActivity.this.finish());
            dialog.show();
        } else
            finish();
    }
}

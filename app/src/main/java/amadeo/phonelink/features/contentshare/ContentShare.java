package amadeo.phonelink.features.contentshare;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import amadeo.phonelink.PreferenceHelpers;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.R;
import amadeo.phonelink.device.network.DeviceManagerHelper;
import amadeo.phonelink.features.Feature;
import amadeo.phonelink.features.FeatureManager;
import amadeo.phonelink.service.network.NetPackage;

public class ContentShare extends Feature {
    private static final String TAG = "PhoneLink ContentShare";

    //region Constants
    public static final String PREFIX = "contentshare_";
    public static final String NAME = "ContentShare";
    public static final String DISPLAY_NAME = "Content Share";

    public static final String TYPE_URL = PREFIX + "url";
    public static final String TYPE_IMG = PREFIX + "image";
    public static final String TYPE_FILE = PREFIX + "file";

    public static final String KEY_PENDING_DATA = "pendingData";

    public static final String KEY_TYPE = "type";

    public static final String KEY_TYPE_URL_URL = "url";
    public static final String KEY_TYPE_URL_DATE = "date";

//    public static final String KEY_TYPE_IMG_BASE64 = "base64";
//    public static final String KEY_TYPE_IMG_MODE = "mode";
//    public static final String KEY_TYPE_IMG_MODE_SHARE = "share";
//    public static final String KEY_TYPE_IMG_MODE_SHARE_AND_CB = "shareandcb";
//    public static final String KEY_TYPE_IMG_MODE_CLIPBOARD = "clipboard";
//    public static final String KEY_TYPE_IMG_DATE = KEY_TYPE_URL_DATE;

    public static final String KEY_TYPE_FILE_BASE64 = "payload";
    public static final String KEY_TYPE_FILE_NAME = "name";
    public static final String KEY_TYPE_FILE_DATE = KEY_TYPE_URL_DATE;
    public static final String KEY_TYPE_FILE_MODE = "mode";
    public static final String KEY_TYPE_FILE_MODE_SHARE = "share";
    public static final String KEY_TYPE_FILE_MODE_SHARE_AND_CB = "shareandcb";
    public static final String KEY_TYPE_FILE_MODE_CLIPBOARD = "clipboard";
    //endregion

    public ContentShare(Context context) {
        super(context);
    }

    @Override
    public String getPrefix() {
        return PREFIX;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDisplayName() {
        return DISPLAY_NAME;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onConnected() {
        JSONArray pendingData = getPendingData();

        for (int i = 0; i < pendingData.length(); i++) {
            try {
                NetPackage sharedItemPackage = NetPackage.deserialize((String) pendingData.get(i));
                DeviceManagerHelper.sendNetPackageToAll(getContext(), sharedItemPackage);
            } catch (JSONException e) {
                PhoneLinkUtils.logW(TAG, "exception retrieving shared item %s", e.getMessage());
            }
        }

        getPreferences().edit().putString(KEY_PENDING_DATA, "").apply();
    }

    @Override
    public void onNetPackageReceived(NetPackage netPackage) {

    }

    @Override
    public void onNetPackageFailed(NetPackage netPackage) {
        addToPending(netPackage);
    }

    @Override
    public void onStop() {

    }

    @Override
    public boolean hasSettings() {
        return true;
    }

    private JSONArray getPendingData() {
        JSONArray pendingData;
        try {
            pendingData = PreferenceHelpers.optJSONArray(getPreferences(), KEY_PENDING_DATA);
        } catch (JSONException e) {
            PhoneLinkUtils.logW(TAG, "exception retrieving pending data, clearing: %s", e.getMessage());
            getPreferences().edit().putString(KEY_PENDING_DATA, "[]").apply();
            pendingData = getPendingData();
        }

        return pendingData;
    }

    private void addToPending(NetPackage netPackage) {
        JSONArray pendingData = getPendingData();
        pendingData.put(netPackage.toString());

        //TODO: add gui to ask if user wants to add given shared content to pending
        //TODO: remove this handler looper stuff
        new Handler(Looper.getMainLooper()).post(() -> Toast.makeText(getContext(), R.string.added_to_pending, Toast.LENGTH_SHORT).show());
    }

    public void share(NetPackage netPackage) {
        //TODO: rework how feature manager works
        if (FeatureManager.isStarted()) {
            DeviceManagerHelper.sendNetPackageToAll(getContext(), netPackage);
        } else {
            addToPending(netPackage);
        }
    }
}

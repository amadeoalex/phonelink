package amadeo.phonelink.features;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.service.network.NetPackage;

/**
 * Created by Amadeo on 2017-12-09.
 */

public abstract class Feature {
    public static final String TAG = "Feature";

    //region Constants
    public static final String KEY_FEATURE_PREF = "featurePreferences";

    public static final String ARG_NAME = "featureName";
    public static final String ARG_DISPLAY_NAME = "displayName";
    //endregion

    private final WeakReference<Context> contextWeakReference;

    private SharedPreferences preferences = null;

    public Feature(Context context) {
        this.contextWeakReference = new WeakReference<>(context);
    }

    public abstract String getPrefix();

    public abstract String getName();

    public abstract String getDisplayName();

    public abstract void onCreate();

    public abstract void onStart();

    public abstract void onConnected();

    public abstract void onNetPackageReceived(NetPackage netPackage);

    public abstract void onNetPackageFailed(NetPackage netPackage);

    public abstract void onStop();

    public abstract boolean hasSettings();

    public Context getContext() {
        return contextWeakReference.get();
    }

    public SharedPreferences getPreferences() {
        if (preferences == null) {
            preferences = PreferenceManager.getDefaultSharedPreferences(contextWeakReference.get());
        }

        return preferences;
    }
}
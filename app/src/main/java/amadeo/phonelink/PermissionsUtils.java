package amadeo.phonelink;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;

/**
 * Created by Amadeo on 2018-09-05.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class PermissionsUtils {
    public static final int REQUEST_RETURN_CODE = 0;

    private static final String[] requiredPermissions = new String[]{
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_SMS,
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_CONTACTS};

    public static synchronized String[] getNotGrantedPermission(Context context) {
        ArrayList<String> notGrantedPermissions = new ArrayList<>();

        for (String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                notGrantedPermissions.add(permission);
            }
        }

        return notGrantedPermissions.toArray(new String[0]);
    }

    public static synchronized boolean checkPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static synchronized void checkAndRequestPermissions(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permissions = getNotGrantedPermission(activity);
            if (permissions.length > 0)
                ActivityCompat.requestPermissions(activity, permissions, REQUEST_RETURN_CODE);
        }
    }

    public static synchronized void checkAndRequestNotificationAccess(Activity activity) {
        String notificationListeners = Settings.Secure.getString(activity.getContentResolver(), "enabled_notification_listeners");

        if (notificationListeners==null || !notificationListeners.contains(activity.getApplicationContext().getPackageName())) {
            activity.startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }
}

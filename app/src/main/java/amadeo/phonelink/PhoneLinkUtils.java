package amadeo.phonelink;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.OpenableColumns;
import android.util.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.Inflater;

import amadeo.phonelink.device.DeviceBase;

/**
 * Created by Amadeo on 2018-01-03.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class PhoneLinkUtils {
    private static final String TAG = "PhoneLink Utils";

    private static final String FIRST_RUN_KEY = "APP_FIRST_RUN";

    private static final Random random = new Random();

    //TODO: change to use inEmulator() function before "release"
    private static boolean logEnabled = true;

    private static final PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

    public static void setLogEnabled() {
        logEnabled = true;
    }

    public static boolean inEmulator() {
        return Build.FINGERPRINT.startsWith("generic") || Build.FINGERPRINT.startsWith("unknown") || Build.MODEL.contains("google_sdk") || Build.MODEL.contains("Emulator") || Build.MODEL.contains("Android SDK built for x86") || Build.MANUFACTURER.contains("Genymotion") || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic")) || "google_sdk".equals(Build.PRODUCT);
    }

    public static synchronized boolean firstRun(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return !sharedPreferences.contains("APP_FIRST_RUN");
    }

    public static synchronized void firstRunFinished(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putBoolean(FIRST_RUN_KEY, true).apply();
    }

    private static String format(String message, Object... args) {
        return String.format(message, args);
    }

    public static synchronized void logI(String TAG, String message, Object... args) {
        if (logEnabled) {
            if (args != null)
                Log.i(TAG, format(message, args));
            else
                Log.i(TAG, message);
        }
    }

    public static synchronized void logW(String TAG, String message, Object... args) {
        if (logEnabled) {
            if (args != null)
                Log.w(TAG, format(message, args));
            else
                Log.w(TAG, message);
        }
    }

    public static synchronized String stripNumber(String number) {
        if (number.matches(".*[a-zA-Z]+.*"))
            return number;

        try {
            Phonenumber.PhoneNumber numberProto = phoneNumberUtil.parse(number, "PL");

            long nationalNumber = numberProto.getNationalNumber();

            return String.valueOf(nationalNumber);
        } catch (NumberParseException e) {
            PhoneLinkUtils.logI(TAG, "TRIED: %s", number);
            e.printStackTrace();
        }

        return "EXCEPTION";
    }

    public static synchronized long removeHundreds(long number) {
        return (number / 1000) * 1000;
    }

    public static synchronized JSONArray reverseOrder(JSONArray jsonArray) {
        if (jsonArray.length() == 0)
            return jsonArray;

        JSONArray reversed = new JSONArray();
        try {
            for (int i = jsonArray.length() - 1; i >= 0; i--)
                reversed.put(jsonArray.get(i));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return reversed;
    }

    public static synchronized int getRandomInt(int min, int max) {
        return random.nextInt((max - min) + 1) + min;
    }

    public static byte[] readAllBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];

        int read = 0;
        while ((read = inputStream.read(buffer)) != -1) {
            byteArrayOutputStream.write(buffer, 0, read);
        }

        return byteArrayOutputStream.toByteArray();
    }

    public static String getFileNameFromURI(Context context, Uri uri) {
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(uri, null, null, null, null);

        if (cursor == null) {
            PhoneLinkUtils.logW(TAG, "unknown file name from uri: %s", uri);
            return null;
        }

        cursor.moveToFirst();
        String fileName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
        cursor.close();

        return fileName;
    }

    public static String getDeviceType(Context context){
        PackageManager pm = context.getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY) ? DeviceBase.TYPE_PHONE : DeviceBase.TYPE_TABLET;
    }
}

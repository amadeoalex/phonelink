package amadeo.phonelink.service;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.content.LocalBroadcastManager;

import java.util.ArrayList;

import amadeo.phonelink.PhoneLinkUtils;

/**
 * Created by Amadeo on 2018-09-03.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class NotificationListener extends NotificationListenerService {
    private final static String TAG = "PhoneLink NotificationL";

    public static final String BROADCAST_ACTION = "amadeo.intent.NOTIFICATION_LISTENER";
    public static final String COMMAND_REMOVE_SMS_NOTIFICATIONS = "remove_sms_notification";

    private NotificationBroadcastListener notificationBroadcastListener;

    private String SMS_APP_PACKAGE;

    private final ArrayList<String> wantedList = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();

        notificationBroadcastListener = new NotificationBroadcastListener();
        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(notificationBroadcastListener, intentFilter);
        PhoneLinkUtils.logI(TAG, "listener registered");

        SMS_APP_PACKAGE = Telephony.Sms.getDefaultSmsPackage(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (notificationBroadcastListener != null)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(notificationBroadcastListener);
    }

    private String getNotificationNewestText(StatusBarNotification sbn) {
        //TODO: find out whether there is less hacky way to accomplish this
        String tickerText = sbn.getNotification().tickerText.toString();

        return tickerText.substring(tickerText.indexOf(": ") + 2);
    }

    private void cancelNotification(StatusBarNotification sbn) {
        if (Build.VERSION.SDK_INT < 21) {
            cancelNotification(sbn.getPackageName(), sbn.getTag(), sbn.getId());
        } else {
            cancelNotification(sbn.getKey());
        }
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        super.onNotificationPosted(sbn);

        //PhoneLinkUtils.logI(TAG, "Created: ID: %s \t %s \t %s", sbn.getId(), sbn.getNotification().tickerText, sbn.getPackageName());


        if (sbn.getPackageName().equals(SMS_APP_PACKAGE)) {
            String message = getNotificationNewestText(sbn);
            if (wantedList.contains(message)) {
                PhoneLinkUtils.logI(TAG, "%s is wanted, removing", message);
                wantedList.remove(message);
                cancelNotification(sbn);
            }
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);

        //PhoneLinkUtils.logI(TAG, "Removed: ID: %s \t %s \t %s", sbn.getId(), sbn.getNotification().tickerText, sbn.getPackageName());
    }


    private final class NotificationBroadcastListener extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                Bundle bundle = intent.getExtras();
                if (bundle == null) {
                    PhoneLinkUtils.logI(TAG, "bundle is null");
                    return;
                }

                String command = bundle.getString(BROADCAST_ACTION, "");
                switch (command) {
                    case COMMAND_REMOVE_SMS_NOTIFICATIONS:
                        String targetMessage = bundle.getString(Notification.EXTRA_TEXT);
                        if (targetMessage == null) {
                            PhoneLinkUtils.logI(TAG, "no target message, command ignored");
                            break;
                        }

                        targetMessage = targetMessage.replace("\n", " ");

                        boolean found = false;
                        for (StatusBarNotification sbn : getActiveNotifications()) {
                            if (sbn.getPackageName().equals(SMS_APP_PACKAGE)) {
                                String message = getNotificationNewestText(sbn);
                                if (message.equals(targetMessage)) {
                                    cancelNotification(sbn);
                                    found = true;
                                } else
                                    PhoneLinkUtils.logI(TAG, "mismatch: M: %s != TM: %s", message, targetMessage);
                            }
                        }

                        if (!found) {
                            PhoneLinkUtils.logI(TAG, "%s added to wanted list", targetMessage);
                            wantedList.add(targetMessage);
                        }

                        break;
                    default:
                        PhoneLinkUtils.logI(TAG, "unknown command");
                        break;
                }
            }
        }
    }
}

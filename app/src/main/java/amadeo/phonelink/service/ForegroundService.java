package amadeo.phonelink.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.R;
import amadeo.phonelink.SecurityUtils;
import amadeo.phonelink.device.DeviceManager;
import amadeo.phonelink.features.FeatureManager;
import amadeo.phonelink.features.contentshare.ContentShare;
import amadeo.phonelink.features.smsnotifications.SMSNotifications;
import amadeo.phonelink.service.network.NetworkBroadcastReceiver;
import amadeo.phonelink.service.network.UDPServer;
import amadeo.phonelink.ui.MainActivity;

/**
 * Created by Amadeo on 2018-05-23.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class ForegroundService extends Service {
    private static final String TAG = "PhoneLink ForegroundService";

    private static final String MAIN_NOTIFICATION_CHANNEL = "phone_link_main";
    public static final String PAIRING_NOTIFICATION_CHANNEL = "phone_link_pair";

    public static final int MAIN_NOTIFICATION_ID = 1234;
    public static final int PAIR_NOTIFICATION_ID = 1235;

    public NotificationManager notificationManager;
    public NotificationCompat.Builder notificationCompatBuilder;
    public Notification notification;

    public UDPServer udpServer;

    private NetworkBroadcastReceiver networkBroadcastReceiver;
    private IntentFilter intentFilter;

    public interface WorkCallback {
        void onStart(ForegroundService service);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        PhoneLinkUtils.logI(TAG, "foreground service constructor");

        PhoneLinkUtils.setLogEnabled();

        prepareNotification();
        startForeground(MAIN_NOTIFICATION_ID, notification);

        DeviceManager.getInstance(getApplicationContext()).initDevices();

        initNetworkBroadcastListener();
        initUDPServer();
        initFeatures();
        SecurityUtils.init(getApplicationContext());

        registerReceiver(networkBroadcastReceiver, intentFilter);
    }

    private void prepareNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            PhoneLinkUtils.logI(TAG, "creating notification");

            NotificationChannel mainNotificationChannel = new NotificationChannel(MAIN_NOTIFICATION_CHANNEL, getString(R.string.notificationMainChannel), NotificationManager.IMPORTANCE_MIN);
            mainNotificationChannel.setDescription(getString(R.string.notificationMainChannelDescription));

            NotificationChannel pairNotificationChannel = new NotificationChannel(PAIRING_NOTIFICATION_CHANNEL, getString(R.string.notificationPairChannel), NotificationManager.IMPORTANCE_HIGH);
            pairNotificationChannel.setDescription(getString(R.string.notificationPairChannelDescription));

            notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mainNotificationChannel);
                notificationManager.createNotificationChannel(pairNotificationChannel);
            }
        } else
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        Intent contentIntent = new Intent(this, MainActivity.class);
        contentIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        contentIntent.setAction("amadeo.intent.CONTENT");
        contentIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(this, 0, contentIntent, 0);

        Intent stopServiceIntent = new Intent(this, MainActivity.class);
        stopServiceIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        stopServiceIntent.setAction("amadeo.intent.STOP");
        PendingIntent stopServicePendingIntent = PendingIntent.getActivity(this, 0, stopServiceIntent, 0);

        notificationCompatBuilder = new NotificationCompat.Builder(this, MAIN_NOTIFICATION_CHANNEL)
                .setContentTitle(getString(R.string.notificationMainTitle))
                .setContentText(getString(R.string.notificationMainText))
                .setSmallIcon(R.drawable.ic_notification_main)
                .setShowWhen(false)
                .setContentIntent(contentPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_MIN)
                .addAction(0, getString(R.string.notificationMainStopService), stopServicePendingIntent)
                .setOnlyAlertOnce(true);

        notification = notificationCompatBuilder.build();
    }

    private void initNetworkBroadcastListener() {
        intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        networkBroadcastReceiver = new NetworkBroadcastReceiver();
    }

    private void initUDPServer() {
        udpServer = new UDPServer(getApplicationContext());
        udpServer.start();
    }

    private void initFeatures() {
        FeatureManager.init(getApplicationContext());
        FeatureManager.registerFeature(new SMSNotifications(getApplicationContext()));
        FeatureManager.registerFeature(new ContentShare(getApplicationContext()));
    }


    private static final ArrayList<WorkCallback> workCallbacks = new ArrayList<>();
    private final static Lock mutex = new ReentrantLock(true);

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        PhoneLinkUtils.logI(TAG, "onStartCommand()");

        mutex.lock();
        try {
            for (WorkCallback workCallback : workCallbacks) {
                workCallback.onStart(this);
            }
            workCallbacks.clear();
        } finally {
            mutex.unlock();
        }

        return START_STICKY;
    }

    public static void CreateWork(Context context, WorkCallback workCallback) {
        Thread thread = new Thread(() -> {
            if (workCallback != null) {
                mutex.lock();
                try {
                    workCallbacks.add(workCallback);
                    PhoneLinkUtils.logI(TAG, "thread: %d, work added", Thread.currentThread().getId());
                } finally {
                    mutex.unlock();
                }
            }
            Intent intent = new Intent(context, ForegroundService.class);
            ContextCompat.startForegroundService(context, intent);
        });
        thread.setName("CreateWork thread");
        thread.start();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        PhoneLinkUtils.logI(TAG, "foreground service destructor");

        try {
            unregisterReceiver(networkBroadcastReceiver);
        } catch (IllegalArgumentException e) {
            PhoneLinkUtils.logI(TAG, "network receiver not registered");
        }

        FeatureManager.onDestroy();

        Thread networkCleanupThread = new Thread(() -> {
            DeviceManager.getInstance(getApplicationContext()).onDestroy();

            if (udpServer != null && udpServer.isAlive() && !udpServer.isInterrupted()) {
                udpServer.interrupt();
                try {
                    udpServer.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        networkCleanupThread.start();
        try {
            networkCleanupThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        PhoneLinkUtils.logI(TAG, "onDestroy finished");
    }
}

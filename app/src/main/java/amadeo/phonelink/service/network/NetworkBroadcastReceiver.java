package amadeo.phonelink.service.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;

import amadeo.phonelink.NetworkUtils;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.device.DeviceManager;
import amadeo.phonelink.features.FeatureManager;
import amadeo.phonelink.service.ForegroundService;

/**
 * Created by Amadeo on 2018-07-01.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class NetworkBroadcastReceiver extends BroadcastReceiver {
    private final String TAG = "PhoneLink NetBRec";

    private static WifiInfo lastWiFiInfo = null;

    public NetworkBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            PhoneLinkUtils.logI(TAG, "connectivityManager is null!");
            return;
        }
        if (PhoneLinkUtils.inEmulator()) {
            PhoneLinkUtils.logI(TAG, "on trusted network - emulator");
            FeatureManager.startFeatures();
            return;
        }

        final NetworkInfo wifiNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isConnected = wifiNetworkInfo != null && wifiNetworkInfo.isConnectedOrConnecting();
        if (isConnected) {
            WifiInfo wifiInfo = NetworkUtils.getWiFiInfo(context);
            if (onNewWifi(wifiInfo)) {
                DeviceManager.getInstance(context).onConnected(wifiInfo);
                PhoneLinkUtils.logI(TAG, "new network");
                if (DeviceManager.getInstance(context).onAnyTrustedWiFi()) {
                    PhoneLinkUtils.logI(TAG, "on trusted network");
                    FeatureManager.startFeatures();
                }
                lastWiFiInfo = wifiInfo;
            }
        } else {
            PhoneLinkUtils.logI(TAG, "NO");
            FeatureManager.stopFeatures();
            DeviceManager.getInstance(context).onDisconnected();
            lastWiFiInfo = null;
        }
//        boolean isConnected = wifi != null && wifi.isConnectedOrConnecting() ||
//                mobile != null && mobile.isConnectedOrConnecting();
//        if (isConnected) {
//            Log.d("Network Available ", "YES");
//        } else {
//            Log.d("Network Available ", "NO");
//        }
    }

    private boolean onNewWifi(WifiInfo wifiInfo) {
        return lastWiFiInfo == null || !lastWiFiInfo.getBSSID().equals(wifiInfo.getBSSID()) || !lastWiFiInfo.getSSID().equals(wifiInfo.getSSID());
    }
}
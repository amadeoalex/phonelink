package amadeo.phonelink.service.network;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by Amadeo on 2017-10-25.
 */

public class NetPackage {
    private JSONObject jsonObject;

    public NetPackage() {
    }

    public NetPackage(String type) {
        jsonObject = new JSONObject();
        try {
            this.jsonObject.put("__type__", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Deprecated
    public NetPackage(BufferedReader bufferedReader) throws IOException {
        try {
            String jsonString = bufferedReader.readLine();

            this.jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            throw new IOException("json string is null");
        }
    }

    public static NetPackage deserialize(String serialized) {
        try {
            JSONObject jsonObject = new JSONObject(serialized);

            NetPackage netPackage = new NetPackage();
            netPackage.jsonObject = jsonObject;

            return netPackage;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void setType(String type) {
        try {
            this.jsonObject.put("__type__", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String type() {
        return this.jsonObject.optString("__type__");
    }

    public void setString(String key, String value) {
        try {
            this.jsonObject.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getString(String key) {
        return this.jsonObject.optString(key, null);
    }

    public void setJSONObject(String key, JSONObject jsonObject) {
        try {
            this.jsonObject.put(key, jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject getJSONObject(String key) {
        return this.jsonObject.optJSONObject(key);
    }

    public void setJSONArray(String key, JSONArray jsonArray) {
        try {
            this.jsonObject.put(key, jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONArray getJSONArray(String key) {
        return this.jsonObject.optJSONArray(key);
    }

    public void setInt(String key, int integer) {
        try {
            this.jsonObject.put(key, integer);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getInt(String key) {
        return this.jsonObject.optInt(key);
    }

    public int size() {
        return jsonObject.length() - 1;
    }

    @Override
    public String toString() {
        return jsonObject.toString();
    }
}


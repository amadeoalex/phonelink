package amadeo.phonelink.service.network;

import android.net.wifi.WifiInfo;

public interface ConnectionChangeCallback {
    void onConnected(WifiInfo wifiInfo);

    void onDisconnected();
}

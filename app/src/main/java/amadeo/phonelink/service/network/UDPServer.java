package amadeo.phonelink.service.network;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import amadeo.phonelink.PhoneLinkBroadcastReceiver;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.R;
import amadeo.phonelink.device.DeviceBase;
import amadeo.phonelink.service.ForegroundService;
import amadeo.phonelink.ui.MainActivity;

/**
 * Created by Amadeo on 2018-06-24.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class UDPServer extends Thread {
    private static final String TAG = "PhoneLink UDPServer";

    public static final String TYPE_PAIR = "amadeo.phonelink.package.PAIR";
    public static final String TYPE_LOOKUP = "amadeo.phonelink.package.LOOKUP";
    public static final String TYPE_LOOKUP_RESPONSE = "amadeo.phonelink.package.LOOKUP_RESPONSE";

    private final WeakReference<Context> contextWeakReference;

    private DatagramSocket serverSocket;
    private boolean running = true;

    public UDPServer(Context context) {
        this.contextWeakReference = new WeakReference<>(context);
    }

    @Override
    public void run() {
        try {
            serverSocket = new DatagramSocket(4848);

            while (running) {
                byte[] buffer = new byte[1024 * 256];
                DatagramPacket receivedPacket = new DatagramPacket(buffer, buffer.length);

                serverSocket.receive(receivedPacket);
                DatagramPacket responsePacket = packetReceived(receivedPacket);

                if (responsePacket != null)
                    serverSocket.send(responsePacket);
            }

            serverSocket.close();
        } catch (IOException e) {
            if (running)
                e.printStackTrace();
        }
    }

    private DatagramPacket packetReceived(DatagramPacket packet) {
        NetPackage netPackage = NetPackage.deserialize(new String(packet.getData()).trim());
        if (netPackage == null) {
            PhoneLinkUtils.logI(TAG, "error deserializing netPackage");
            return null;
        }

        String receivedID = netPackage.getString(DeviceBase.KEY_DEVICE_ID);
        if (receivedID != null && receivedID.equals(Build.MODEL))
            return null;

        switch (netPackage.type()) {
            case TYPE_LOOKUP:
                PhoneLinkUtils.logI(TAG, "lookup package received from: %s", packet.getAddress().getHostAddress());
                NetPackage responseNetPackage = new NetPackage(TYPE_LOOKUP_RESPONSE);
                responseNetPackage.setString(DeviceBase.KEY_DEVICE_ID, Build.MODEL);
                responseNetPackage.setString(DeviceBase.KEY_TYPE, PhoneLinkUtils.getDeviceType(contextWeakReference.get()));

                return new DatagramPacket(responseNetPackage.toString().getBytes(), responseNetPackage.toString().getBytes().length, packet.getAddress(), packet.getPort());
            case TYPE_PAIR:
                PhoneLinkUtils.logI(TAG, "pair package received from %s", packet.getAddress().getHostAddress());

                pairingPacketReceived(packet, netPackage);
                return null;
            default:
                PhoneLinkUtils.logW(TAG, "unhandled package received: %s", netPackage.type());

                return null;
        }
    }

    @Override
    public void interrupt() {
        running = false;
        serverSocket.close();
    }

    private void pairingPacketReceived(DatagramPacket packet, NetPackage netPackage) {
        Context context = contextWeakReference.get();

        netPackage.setString(DeviceBase.KEY_INET_ADDRESS, packet.getAddress().getHostAddress());
        netPackage.setInt("isClient", 1);

        Intent acceptedIntent = new Intent(context, MainActivity.class);
        acceptedIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        acceptedIntent.setAction("amadeo.intent.action.PAIR_NOTIFICATION");
        acceptedIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        acceptedIntent.putExtra("netPackage", netPackage.toString());
        acceptedIntent.putExtra("result", true);

        Intent rejectedIntent = new Intent(context, PhoneLinkBroadcastReceiver.class);
        acceptedIntent.setAction("amadeo.intent.action.PAIR_NOTIFICATION");
        acceptedIntent.putExtra("result", false);

        PendingIntent acceptedPendingIntent = PendingIntent.getActivity(context, 12, acceptedIntent, PendingIntent.FLAG_ONE_SHOT);
        PendingIntent rejectedPendingIntent = PendingIntent.getBroadcast(context, 13, rejectedIntent, PendingIntent.FLAG_ONE_SHOT);

        Resources resources = context.getResources();

        Notification notification = new NotificationCompat.Builder(context, ForegroundService.PAIRING_NOTIFICATION_CHANNEL)
                .setContentTitle(resources.getString(R.string.pairing_title, netPackage.getString(DeviceBase.KEY_DEVICE_ID)))
                .setSmallIcon(R.drawable.ic_notification_pairing)
                .addAction(R.drawable.ic_accept, resources.getString(R.string.accept), acceptedPendingIntent)
                .addAction(R.drawable.ic_reject, resources.getString(R.string.reject), rejectedPendingIntent)
                .setAutoCancel(true)
                .setWhen(0)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(ForegroundService.PAIR_NOTIFICATION_ID, notification);
    }

}

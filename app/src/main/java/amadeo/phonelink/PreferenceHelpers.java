package amadeo.phonelink;

import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PreferenceHelpers {

    /**
     * Returns JSONObject with given key.
     * If requested object does not exists, creates a new one, inserts and returns it.
     *
     * @param source
     * @param key
     * @return
     * @throws JSONException
     */
    public static JSONObject optJSONObject(SharedPreferences source, String key) throws JSONException {
        if (!source.contains(key))
            source.edit().putString(key, "").apply();

        return new JSONObject(source.getString(key, ""));
    }

    /**
     * Returns JSONArray with given key.
     * If requested array does not exists, creates a new one, inserts and returns it.
     *
     * @param source
     * @param key
     * @return
     * @throws JSONException
     */
    public static JSONArray optJSONArray(SharedPreferences source, String key) throws JSONException {
        if (!source.contains(key))
            source.edit().putString(key, "[]").apply();

        return new JSONArray(source.getString(key, "[]"));
    }

    /**
     * Returns boolean with given key.
     * If requested boolean does not exists, creates a new one, inserts and returns it.
     *
     * @param source
     * @param key
     * @param fallback
     * @return
     * @throws JSONException
     */
    public static boolean optBoolean(SharedPreferences source, String key, boolean fallback) throws JSONException {
        if (!source.contains(key))
            source.edit().putBoolean(key, fallback).apply();

        return source.getBoolean(key, fallback);
    }

    /**
     * Returns String with given key.
     * If requested String does not exists, creates a new one, inserts and returns it.
     *
     * @param source
     * @param key
     * @param fallback
     * @return
     * @throws JSONException
     */
    public static String optString(SharedPreferences source, String key, String fallback) throws JSONException {
        if (!source.contains(key))
            source.edit().putString(key, fallback).apply();

        return source.getString(key, fallback);
    }

    /**
     * Returns int with given key.
     * If requested int does not exists, creates a new one, inserts and returns it.
     *
     * @param source
     * @param key
     * @param fallback
     * @return
     * @throws JSONException
     */
    public static int optInt(SharedPreferences source, String key, int fallback) throws JSONException {
        if (!source.contains(key))
            source.edit().putInt(key, fallback).apply();

        return source.getInt(key, fallback);
    }
}

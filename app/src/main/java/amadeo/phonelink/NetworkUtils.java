package amadeo.phonelink;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.util.ArrayList;

/**
 * Created by Amadeo on 2018-07-01.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class NetworkUtils {
    private static final String TAG = "PhoneLink NetUtils";

    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return connectivityManager == null ? null : connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
    }

    public static boolean connectedToWiFi(Context context) {
        NetworkInfo networkInfo = getNetworkInfo(context);
        return networkInfo != null && networkInfo.isConnected() || PhoneLinkUtils.inEmulator();
    }

    public static WifiInfo getWiFiInfo(Context context) {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        return wifiManager == null ? null : wifiManager.getConnectionInfo();
    }
}

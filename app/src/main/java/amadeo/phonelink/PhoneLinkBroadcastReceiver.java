package amadeo.phonelink;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;

import java.net.InetAddress;
import java.net.UnknownHostException;

import amadeo.phonelink.device.DeviceBase;
import amadeo.phonelink.service.ForegroundService;
import amadeo.phonelink.service.network.NetPackage;
import amadeo.phonelink.ui.drawer.pairing.PairingAsyncTask;

/**
 * Created by Amadeo on 2017-10-16.
 */

public class PhoneLinkBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "PhoneLink BR";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            Bundle bundle = intent.getExtras();

            if (action != null) {

                switch (action) {
                    case "android.intent.action.BOOT_COMPLETED":
                        ForegroundService.CreateWork(context, service -> PhoneLinkUtils.logI(TAG, "trigger!"));
                        break;
                    case "amadeo.intent.action.PAIR_NOTIFICATION":
                        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
                        notificationManagerCompat.cancel(ForegroundService.PAIR_NOTIFICATION_ID);

                        try {
                            if (bundle != null && bundle.getBoolean("result")) {
                                InetAddress inetAddress = InetAddress.getByName(bundle.getString(DeviceBase.KEY_INET_ADDRESS));
                                NetPackage netPackage = NetPackage.deserialize(bundle.getString("netPackage"));

                                new PairingAsyncTask(context, inetAddress, true, netPackage, null, null).execute();
                            }
                        } catch (UnknownHostException e) {
                            e.printStackTrace();
                        }


                        break;
                    default:
                        PhoneLinkUtils.logW(TAG, "received unknown action: %s", action);
                        break;
                }

            }
        }
    }
}
